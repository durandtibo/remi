
# add mosek.jar
echo "@ add mosek.jar"
mvn install:install-file -Dfile=/local/durandt/mosek/7/tools/platform/linux64x86/bin/mosek.jar -DgroupId=com.mosek -DartifactId=mosek7 -Dversion=1.0 -Dpackaging=jar
echo "@ add progressbar"
mvn install:install-file -Dfile=jars/progressbar-0.0.1-SNAPSHOT.jar -DgroupId=fr.durandt -DartifactId=progressbar -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar

echo "@ generate package"
mvn package

echo "@ generate source-jar"
mvn source:jar

