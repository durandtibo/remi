package fr.lip6.durandt.wsl.remi.util.model;

import java.io.Serializable;

public abstract class StructuredModel<X, Y> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5274537024453704837L;

	public abstract Y predict(X x);
	
	public abstract double valueOf(X x, Y y);
	
	public double valueOf(X x) {
		return valueOf(x, predict(x));
	}

}
