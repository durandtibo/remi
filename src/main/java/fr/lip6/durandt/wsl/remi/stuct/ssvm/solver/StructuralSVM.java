package fr.lip6.durandt.wsl.remi.stuct.ssvm.solver;

import java.io.Serializable;
import java.util.List;

import fr.lip6.durandt.wsl.remi.stuct.ssvm.model.PrimalStructuralSVM;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;

public class StructuralSVM<X, Y> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8854814607679696305L;

	private StructuralSVMSolver<X, Y> solver;

	public StructuralSVM(StructuralSVMSolver<X, Y> solver) {
		super();
		this.solver = solver;
	}

	public PrimalStructuralSVM<X, Y> train(
			PrimalStructuralSVM<X, Y> model,
			List<LabeledObject<X, Y>> data,
			List<LabeledObject<X, Y>> testData) {

		final int verbose = solver.getOptions().getVerbose();

		// display some informations
		if (verbose >= 0) {
			System.out.println("\n----------------------------------------------------------------------------------------------------");
			System.out.println("############################################## SSVM ################################################");
			solver.getOptions().print();
			System.out.println("----------------------------------------------------------------------------------------------------");
			model.initialization(data);
			System.out.println("# " + model);
			System.out.println("----------------------------------------------------------------------------------------------------");
		}

		// optimization
		long startTime = System.currentTimeMillis();
		PrimalStructuralSVM<X, Y> leranedModel = solver.optimize(model, data);
		long endTime = System.currentTimeMillis();

		if (verbose >= 0) {
			System.out.println("End optimization - Time= " + (endTime - startTime) + " ms");
			// evaluate learned model
			solver.evaluate(leranedModel, data, testData);
			System.out.println("----------------------------------------------------------------------------------------------------");
		}

		return leranedModel;
	}

}
