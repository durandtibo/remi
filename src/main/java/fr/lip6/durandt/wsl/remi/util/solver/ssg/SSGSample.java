package fr.lip6.durandt.wsl.remi.util.solver.ssg;

public enum SSGSample {

	PERM ("PERM"),
	UNIFORM ("UNIFORM"),
	ITER ("ITER");

	private String name = "";

	SSGSample(String name){
		this.name = name;
	}

	@Override
	public String toString(){
		return name;
	}

}
