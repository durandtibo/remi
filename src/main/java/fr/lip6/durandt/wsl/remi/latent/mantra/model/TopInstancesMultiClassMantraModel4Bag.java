package fr.lip6.durandt.wsl.remi.latent.mantra.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.Pair;
import fr.lip6.durandt.wsl.remi.util.VectorOps;
import fr.lip6.durandt.wsl.remi.util.data.bag.Bag;

public class TopInstancesMultiClassMantraModel4Bag<X extends Bag<Z>, Z> extends PrimalMantraModel<X, Integer, Integer[]> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3223198033354331251L;

	private int numClasses = 0;

	private String initLatentType = "fixe";

	private double k = 1;

	public TopInstancesMultiClassMantraModel4Bag() {
		this(1);
	}

	public TopInstancesMultiClassMantraModel4Bag(double k) {
		this.k = k;
	}

	@Override
	public void add(double[] a, X x, Integer y, MantraLatent<Integer[]> h) {
		double[] featureMax = sumInstances(x, h.gethMax());
		double[] featureMin = sumInstances(x, h.gethMin());	
		int offset = y * featureMax.length;
		synchronized(a) {
			for(int i=0; i<featureMax.length; i++) {
				a[offset+i] += (featureMax[i] + featureMin[i]);
			}
		}
	}

	@Override
	public void add(double[] a, X x, Integer y, MantraLatent<Integer[]> h, double gamma) {
		double[] featureMax = sumInstances(x, h.gethMax());
		double[] featureMin = sumInstances(x, h.gethMin());	
		int offset = y * featureMax.length;
		synchronized(a) {
			for(int i=0; i<featureMax.length; i++) {
				a[offset+i] += (featureMax[i] + featureMin[i]) * gamma;
			}
		}
	}

	public List<Integer[]> computePrediction(List<LabeledObject<X, Integer>> data) {
		List<Integer[]> predictionsAndLabels = new ArrayList<Integer[]>();
		for(LabeledObject<X, Integer> example : data) {
			Integer[] p = new Integer[2];
			p[0] = predict(example.getPattern()).getLabel();
			p[1] = example.getLabel();
			predictionsAndLabels.add(p);
		}
		return predictionsAndLabels;
	}

	@Override
	public double[] featureMap(X x, Integer y, MantraLatent<Integer[]> h) {
		double[] featureMax = sumInstances(x, h.gethMax());
		double[] featureMin = sumInstances(x, h.gethMin());	
		double[] psi = new double[featureMax.length * numClasses];
		int offset = y * featureMax.length;
		for(int i=0; i<featureMax.length; i++) {
			psi[offset+i] = featureMax[i] + featureMin[i];
		}
		return psi;
	}

	@Override
	public int getDimension(X x, Integer y, MantraLatent<Integer[]> h) {
		return numClasses * x.getInstanceRepresentation(0).length;
	}

	public String getInitLatentType() {
		return initLatentType;
	}

	public int getNumClasses() {
		return numClasses;
	}

	@Override
	public void initialization(List<LabeledObject<X, Integer>> data) {
		if (numClasses == 0) {
			// search the number of classes
			for(int i=0; i<data.size(); i++) {
				numClasses = Math.max(numClasses, data.get(i).getLabel());
			}
			numClasses++;
		}
	}

	@Override
	public MantraLatent<Integer[]> initializeLatent(X x, Integer y) {
		if(initLatentType.compareToIgnoreCase("fixe") == 0) {
			Integer[] init = new Integer[x.getNumberOfInstances()];
			for(int i=0; i<init.length; i++) {
				init[i] = 1;
			}
			return new MantraLatent<Integer[]>(init, init.clone());
		}
		throw new RuntimeException("Error with initLatentType: " + initLatentType);
	}

	@Override
	public double loss(Integer yTruth, Integer yPredict, MantraLatent<Integer[]> hPredict) {
		if (yTruth == yPredict) {
			return 0.0;
		}
		else {
			return 1.0;
		}
	}

	@Override
	public LabelLatent<Integer, MantraLatent<Integer[]>> maxOracle(X x, Integer yStar) {
		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = null;
		MantraLatent<Integer[]> hPredict = null;
		double valmax = -Double.MAX_VALUE;
		for(int y=0; y<numClasses; y++) {
			MantraLatent<Integer[]> h = predict(x, y);
			double val = loss(yStar, y, h) + valueOf(x, y, h);
			if(val>valmax){
				valmax = val;
				yPredict = y;
				hPredict = h;
			}
		}

		long tEnd = System.currentTimeMillis();
		tMaxOracle += (tEnd - tStart);

		return new LabelLatent<Integer, MantraLatent<Integer[]>>(yPredict, hPredict);
	}

	@Override
	public LabelLatent<Integer, MantraLatent<Integer[]>> predict(X x) {
		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = null;
		MantraLatent<Integer[]> hPredict = null;
		double valmax = -Double.MAX_VALUE;
		for(int y=0; y<numClasses; y++) {
			MantraLatent<Integer[]> h = predict(x, y);
			double val = valueOf(x, y, h);
			if(val>valmax){
				valmax = val;
				yPredict = y;
				hPredict = h;
			}
		}

		long tEnd = System.currentTimeMillis();
		tPredict += (tEnd - tStart);

		return new LabelLatent<Integer, MantraLatent<Integer[]>>(yPredict, hPredict);
	}

	@Override
	public MantraLatent<Integer[]> predict(X x, Integer y) {

		int numInstances = x.getNumberOfInstances();

		// compute the score of each instance
		List<Pair<Integer,Double>> scores = new ArrayList<Pair<Integer,Double>>(numInstances);
		for(int h=0; h<numInstances; h++) {
			double val = valueOf(x, y, h);
			scores.add(new Pair<Integer,Double>(h,val));
		}

		Collections.sort(scores, Collections.reverseOrder());

		Integer[] hmax = new Integer[numInstances];
		Integer[] hmin = new Integer[numInstances];
		for(int i=0; i<numInstances; i++) {
			hmax[i] = 0;
			hmin[i] = 0;
		}

		// find the number of instances
		int kmax = -1;
		if (k <= 0) {
			kmax = 1;
		}
		else if (k >= 1) {
			kmax = (int) Math.min(k, numInstances);
		}
		else {
			kmax = (int) k * numInstances;
		}

		for(int i=0; i<kmax; i++) {
			hmax[scores.get(i).getKey()] = 1;
			hmin[scores.get(numInstances - 1 - i).getKey()] = 1;
		}

		return new MantraLatent<Integer[]>(hmax, hmin);
	}

	public void setInitLatentType(String initLatentType) {
		this.initLatentType = initLatentType;
	}

	public void setNumClasses(int numClasses) {
		this.numClasses = numClasses;
	}

	@Override
	public void sub(double[] a, X x, Integer y, MantraLatent<Integer[]> h) {
		double[] featureMax = sumInstances(x, h.gethMax());
		double[] featureMin = sumInstances(x, h.gethMin());	
		int offset = y * featureMax.length;
		synchronized(a) {
			for(int i=0; i<featureMax.length; i++) {
				a[offset+i] -= (featureMax[i] + featureMin[i]);
			}
		}
	}

	protected double[] sumInstances(X x, Integer[] h) {
		final int dim = x.getInstanceRepresentation(0).length;
		final double[] feature = new double[dim];
		int numberOfInstances = 0;
		for(int i=0; i<h.length; i++) {
			if(h[i] != 0) {
				VectorOps.add(feature, x.getInstanceRepresentation(i));
				numberOfInstances++;
			}
		}
		VectorOps.mul(feature, 1./(double)numberOfInstances);
		return feature;
	}

	protected double valueOf(X x, Integer y, Integer h) {
		double[] feature = x.getInstanceRepresentation(h);
		double[] w = getWeights();
		double score = 0;
		int offset = y * feature.length;
		for(int i=0; i<feature.length; i++) {
			score += feature[i] * w[i+offset];
		}
		return score;
	}

	@Override
	public double valueOf(X x, Integer y, MantraLatent<Integer[]> h) {
		double[] featureMax = sumInstances(x, h.gethMax());
		double[] featureMin = sumInstances(x, h.gethMin());	
		int offset = y * featureMax.length;
		double[] w = getWeights();
		double score = 0;
		for(int i=0; i<featureMax.length; i++) {
			score += (featureMax[i] + featureMin[i]) * w[i+offset];
		}
		return score;
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [numClasses=" + numClasses + ", k=" + k + ", initLatentType=" + initLatentType + ", dimension=" + getDimension() + "]";
	}
}
