package fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.cuttingplane;

import java.util.List;

import fr.lip6.durandt.wsl.remi.stuct.ssvm.model.PrimalStructuralSVM;
import fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.StructSVMUtils;
import fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.StructuralSVMLoss;
import fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.StructuralSVMSolver;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.solver.SolverOptions;
import fr.lip6.durandt.wsl.remi.util.solver.cuttingplane.OneSCPOptions;
import fr.lip6.durandt.wsl.remi.util.solver.cuttingplane.OneSCPSolver;

public class StructuralSVMWith1SCP<X, Y> extends StructuralSVMSolver<X, Y> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5051909035685423481L;

	private OneSCPOptions options = null;

	public StructuralSVMWith1SCP(OneSCPOptions options) {
		super();
		this.options = options;
	}

	public StructuralSVMWith1SCP() {
		this(new OneSCPOptions());
	}

	@Override
	public SolverOptions getOptions() {
		return options;
	}

	@Override
	public PrimalStructuralSVM<X, Y> optimize(final PrimalStructuralSVM<X, Y> model, final List<LabeledObject<X, Y>> data) {
		OneSCPSolver<X, Y, PrimalStructuralSVM<X, Y>> solver = new OneSCPSolver<X, Y, PrimalStructuralSVM<X, Y>>(new StructuralSVMLoss<X, Y>(), options);
		solver.optimize(model, data);
		return model;
	}

	@Override
	public double primalObjective(PrimalStructuralSVM<X, Y> model, List<LabeledObject<X, Y>> data) {
		StructSVMUtils<X, Y> util = new StructSVMUtils<X, Y>(options.getnThreads());
		return util.primalObjective(model, data, 1.0, options.getC());
	}

}
