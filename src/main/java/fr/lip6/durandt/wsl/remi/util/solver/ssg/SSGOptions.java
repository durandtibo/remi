package fr.lip6.durandt.wsl.remi.util.solver.ssg;

import fr.lip6.durandt.wsl.remi.util.solver.SolverOptions;

public class SSGOptions extends SolverOptions {

	public static class Builder extends SolverOptions.Builder {

		/**
		 * regularization parameter
		 */
		private double lambda = 1e-4;

		private int numEpochs = 50;

		private int debugMultiplier = 1;

		private SSGSample sample = SSGSample.PERM;

		private boolean doWeightedAveraging = false;


		public SSGOptions build() {
			return new SSGOptions(this);
		}

		public Builder debugMultiplier(int debugMultiplier) {
			this.debugMultiplier = debugMultiplier;
			return this;
		}

		public Builder doWeightedAveraging(boolean doWeightedAveraging) {
			this.doWeightedAveraging = doWeightedAveraging;
			return this;
		}

		public Builder lambda(double lambda) {
			this.lambda = lambda;
			return this;
		}

		public Builder numEpochs(int numEpochs) {
			this.numEpochs = numEpochs;
			return this;
		}

		public Builder sample(SSGSample sample) {
			this.sample = sample;
			return this;
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3126849295403393775L;

	private double lambda = 1e-4;

	private int numEpochs = 50;

	private int debugMultiplier = 1;

	private SSGSample sample = SSGSample.PERM;

	private boolean doWeightedAveraging = true;

	public SSGOptions() {
		this(new SSGOptions.Builder());
	}


	protected SSGOptions(Builder builder) {
		super(builder);
		this.lambda = builder.lambda;
		this.numEpochs = builder.numEpochs;
		this.sample = builder.sample;
		this.doWeightedAveraging = builder.doWeightedAveraging;
		this.debugMultiplier = builder.debugMultiplier;
	}

	public SSGOptions(
			double lambda,
			int numEpochs, 
			SSGSample sample, 
			boolean doWeightedAveraging,
			int debugMultiplier,
			int verbose,
			int seed) {

		super(verbose, seed);
		this.lambda = lambda;
		this.numEpochs = numEpochs;
		this.sample = sample;
		this.doWeightedAveraging = doWeightedAveraging;
		this.debugMultiplier = debugMultiplier;
	}

	public int getDebugMultiplier() {
		return debugMultiplier;
	}

	public double getLambda() {
		return lambda;
	}

	public int getNumEpochs() {
		return numEpochs;
	}

	public SSGSample getSample() {
		return sample;
	}

	public boolean isDoWeightedAveraging() {
		return doWeightedAveraging;
	}

	public void print() {
		StringBuilder sb = new StringBuilder();
		sb.append("# Stochastic (Sub)Gradient Descent options\n");
		sb.append("# lambda=").append(lambda).append("\n");
		sb.append("# sample=").append(sample).append("\n");
		sb.append("# numEpochs=").append(numEpochs).append("\n");
		sb.append("# doWeightedAveraging=").append(doWeightedAveraging).append("\n");
		System.out.print(sb.toString());
		super.print();
	}

	public void setDebugMultiplier(int debugMultiplier) {
		this.debugMultiplier = debugMultiplier;
	}

	public void setDoWeightedAveraging(boolean doWeightedAveraging) {
		this.doWeightedAveraging = doWeightedAveraging;
	}

	public void setLambda(double lambda) {
		this.lambda = lambda;
	}

	public void setNumEpochs(int numEpochs) {
		this.numEpochs = numEpochs;
	}
	
	public void setSample(SSGSample sample) {
		this.sample = sample;
	}

}
