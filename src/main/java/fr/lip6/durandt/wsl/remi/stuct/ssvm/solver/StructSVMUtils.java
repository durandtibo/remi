package fr.lip6.durandt.wsl.remi.stuct.ssvm.solver;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import fr.lip6.durandt.wsl.remi.stuct.ssvm.model.PrimalStructuralSVM;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;

/**
 * Class to compute standard SSVM operations: <br/>
 * - primal objective (c/lambda variants) <br/>
 * - empirical loss <br/>
 * - loss  
 * 
 * @author Thibaut Durand - durand.tibo@gmail.com
 *
 * @param <X>
 * @param <Y>
 */
public class StructSVMUtils<X, Y> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6259757458080273485L;

	/**
	 * number of threads
	 */
	private int nThreads = 1;

	public StructSVMUtils() {
		this(1);
	}

	public StructSVMUtils(int nThreads) {
		super();
		this.nThreads = nThreads;
	}

	/**
	 * 1/n sum_i=1^n delta(yi, yibar) where yibar = max_y w^T phi(xi,y)
	 * @param model
	 * @param data
	 * @return 1/n sum_i=1^n delta(yi, yibar)
	 */
	public double empiricalLoss(final PrimalStructuralSVM<X, Y> model, 
			final List<LabeledObject<X, Y>> data) {

		double loss = 0;

		if(nThreads > 1) { // Multi-threads execution
			ExecutorService executor = Executors.newFixedThreadPool(nThreads);
			List<Future<Double>> futures = new ArrayList<Future<Double>>();
			CompletionService<Double> completionService = new ExecutorCompletionService<Double>(executor);

			for(int i=0 ; i<data.size(); i++) {
				final int n = i;
				futures.add(completionService.submit(new Callable<Double>() {

					public Double call() throws Exception {
						LabeledObject<X, Y> ls = data.get(n);
						Y yPredict = model.predict(ls.getPattern());
						return model.loss(ls.getLabel(), yPredict);
					}
				}));
			}

			for(Future<Double> f : futures) {
				try {
					Double res = f.get();
					if (res != null) {
						loss += res;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			executor.shutdown();
		}
		else { // Mono-thread execution
			for(LabeledObject<X, Y> ls : data) {
				Y yPredict = model.predict(ls.getPattern());
				loss += model.loss(ls.getLabel(), yPredict);
			}
		}
		loss /= (double)data.size();
		return loss;
	}

	public int getnThreads() {
		return nThreads;
	}

	/**
	 * 1/n sum_i=1^n max_y delta(yi, y) + w^T phi(xi,y) - w^T phi(xi,yi)
	 * @param model
	 * @param data
	 * @return
	 */
	public double loss(final PrimalStructuralSVM<X, Y> model,
			final List<LabeledObject<X, Y>> data) {

		double loss = 0;

		if(nThreads > 1) { // Multi-threads execution

			ExecutorService executor = Executors.newFixedThreadPool(nThreads);
			List<Future<Double>> futures = new ArrayList<Future<Double>>();
			CompletionService<Double> completionService = new ExecutorCompletionService<Double>(executor);

			for(int i=0 ; i<data.size(); i++) {
				final int n = i;
				futures.add(completionService.submit(new Callable<Double>() {
					public Double call() throws Exception {
						LabeledObject<X, Y> lo = data.get(n);
						Y yPredict = model.maxOracle(lo.getPattern(), lo.getLabel());
						return model.loss(lo.getLabel(), yPredict) 
								+ model.valueOf(lo.getPattern(), yPredict) 
								- model.valueOf(lo.getPattern(), lo.getLabel());
					}
				}));
			}

			for(Future<Double> f : futures) {
				try {
					Double res = f.get();
					if (res != null) {
						loss += res;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			executor.shutdown();
		}
		else { // Mono-thread execution
			for(LabeledObject<X, Y> lo : data) {
				Y yPredict = model.maxOracle(lo.getPattern(), lo.getLabel());
				loss += model.loss(lo.getLabel(), yPredict) 
						+ model.valueOf(lo.getPattern(), yPredict) 
						- model.valueOf(lo.getPattern(), lo.getLabel());
			}
		}
		loss /= data.size();

		return loss;	
	}

	public double primalObjective(final PrimalStructuralSVM<X, Y> model,
			final List<LabeledObject<X, Y>> data,
			final double lambda,
			final double c) {

		double loss = loss(model, data);
		return 0.5 * lambda * model.norm2() + c * loss;	
	}

	public void setnThreads(int nThreads) {
		this.nThreads = nThreads;
	}

}
