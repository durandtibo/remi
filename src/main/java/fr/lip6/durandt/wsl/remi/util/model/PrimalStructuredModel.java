package fr.lip6.durandt.wsl.remi.util.model;

import fr.lip6.durandt.wsl.remi.util.VectorOps;

public abstract class PrimalStructuredModel<X, Y> extends StructuredModel<X, Y> implements IPrimalModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7098138492975385572L;

	private double[] w = null;

	/**
	 * a = a + psi(x,y)
	 * @param a
	 * @param x
	 * @param y
	 */
	public void add(double[] a, X x, Y y) {
		double[] psi = featureMap(x, y);
		synchronized(a) {
			VectorOps.add(a, psi);
		}
	}

	/**
	 * a = a + gamma * psi(x,y)
	 * @param a
	 * @param x
	 * @param y
	 */
	public void add(double[] a, X x, Y y, double gamma) {
		double[] psi = featureMap(x, y);
		synchronized(a) {
			VectorOps.add(a, psi, gamma);
		}
	}

	public abstract double[] featureMap(X x, Y y);

	@Override
	public int getDimension() {
		if (w == null) 
			return 0;
		else 
			return w.length;
	}

	@Override
	public double[] getWeights() {
		return w;
	}

	@Override
	public void setWeights(double[] weights) {
		w = weights;
	}

	/**
	 * a = a - psi(x,y)
	 * @param a
	 * @param x
	 * @param y
	 */
	public void sub(double[] a, X x, Y y) {
		double[] psi = featureMap(x, y);
		synchronized(a) {
			VectorOps.sub(a, psi);
		}
	}

	public double valueOfW(double[] psi) {
		return VectorOps.dot(getWeights(), psi);
	}

	@Override
	public double valueOf(X x, Y y) {
		return valueOfW(featureMap(x, y));
	}
}
