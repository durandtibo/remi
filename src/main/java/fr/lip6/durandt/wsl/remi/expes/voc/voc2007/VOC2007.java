package fr.lip6.durandt.wsl.remi.expes.voc.voc2007;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.data.bag.DoubleBag;
import fr.lip6.durandt.wsl.remi.util.data.bag.DoubleBagReader;

public class VOC2007 {

	private static String[] objectCategories = new String[]{
			"aeroplane",
			"bicycle",
			"bird",
			"boat",
			"bottle",
			"bus",
			"car",
			"cat",
			"chair",
			"cow",
			"diningtable",
			"dog",
			"horse",
			"motorbike",
			"person",
			"pottedplant",
			"sheep",
			"sofa",
			"train",
	"tvmonitor"};

	/**
	 * 
	 * @return list of the categories of PASCAL VOC
	 */
	public static String[] getCategories() {
		return objectCategories;
	}

	public static String getCategory(int i) {
		return getCategories()[i];
	}

	public static int numObjectCategories() {
		return objectCategories.length;
	}

	public static List<LabeledObject<DoubleBag, Integer[]>> readDoubleBagCSV(String filename, int dim) {

		final File file = new File(filename);
		System.out.println("Read " + file.getAbsolutePath());

		final int numClasses = getCategories().length;

		List<LabeledObject<DoubleBag, Integer[]>> data = null;

		try {
			CSVReader reader = new CSVReader(new FileReader(file));

			// initialize data		
			data = new ArrayList<LabeledObject<DoubleBag, Integer[]>>();

			// read header
			String [] nextLine = reader.readNext();

			// read business and labels
			while ((nextLine = reader.readNext()) != null) {

				// read business id
				String name = nextLine[0];

				// read labels
				Integer[] labels = new Integer[numClasses];
				for(int i=0; i<labels.length; i++) {
					labels[i] = Integer.parseInt(nextLine[1+i]);
				}

				String featureFile = nextLine[21];

				DoubleBag bag = DoubleBagReader.readXMLFile(new File(featureFile), dim, -1, 0);
				bag.setName(name);

				data.add(new LabeledObject<DoubleBag, Integer[]>(bag, labels));
			}

			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("error when reading file " + file.getAbsolutePath());
		}
		System.out.println("number of bags=" + data.size());

		return data;
	}

}
