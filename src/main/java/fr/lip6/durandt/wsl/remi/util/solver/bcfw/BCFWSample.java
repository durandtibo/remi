package fr.lip6.durandt.wsl.remi.util.solver.bcfw;

public enum BCFWSample {

	PERM ("PERM"),
	UNIFORM ("UNIFORM"),
	ITER ("ITER");

	private String name = "";

	BCFWSample(String name){
		this.name = name;
	}

	@Override
	public String toString(){
		return name;
	}

}
