package fr.lip6.durandt.wsl.remi.latent.mantra.model.io;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.lip6.durandt.wsl.remi.latent.mantra.model.MultiClassMantraModel4DoubleBag;
import fr.lip6.durandt.wsl.remi.util.model.io.MultiClassModelIO;

public class MantraModelReader implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6625234010202963946L;

	public static MultiClassMantraModel4DoubleBag readMultiClassModelJSON(String filename) {

		System.out.println("read model: " + filename);
		File file = new File(filename);

		MultiClassMantraModel4DoubleBag model = new MultiClassMantraModel4DoubleBag();
		MultiClassModelIO iomodel = new MultiClassModelIO(model.getNumClasses(), model.getWeights());

		ObjectMapper mapper = new ObjectMapper();

		try {
			iomodel = mapper.readValue(file, MultiClassModelIO.class);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		model.setWeights(iomodel.getWeights());
		model.setNumClasses(iomodel.getNumClasses());

		return model;
	}

}
