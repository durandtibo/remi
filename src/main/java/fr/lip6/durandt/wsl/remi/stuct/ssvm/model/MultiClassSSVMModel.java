package fr.lip6.durandt.wsl.remi.stuct.ssvm.model;

import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabeledObject;

public class MultiClassSSVMModel extends PrimalStructuralSVM<double[], Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5618727180175485635L;

	private int numClasses = 0;

	private long tPredict = 0;

	private long tMaxOracle = 0;

	@Override
	public void add(double[] a, double[] x, Integer y) {
		int offset = y * x.length;
		synchronized(a) {
			for(int i=0; i<x.length; i++) {
				a[offset+i] += x[i];
			}
		}
	}

	@Override
	public void add(double[] a, double[] x, Integer y, double gamma) {
		int offset = y * x.length;
		synchronized(a) {
			for(int i=0; i<x.length; i++) {
				a[offset+i] += gamma * x[i];
			}
		}
	}

	@Override
	public double[] featureMap(double[] x, Integer y) {
		double[] psi = new double[x.length * numClasses];
		int offset = y*x.length;
		for(int i=0; i<x.length; i++) {
			psi[offset+i] = x[i];
		}
		return psi;
	}

	@Override
	public int getDimension(double[] x, Integer y) {
		return x.length * numClasses;
	}

	public int getNumClasses() {
		return numClasses;
	}

	public long gettMaxOracle() {
		return tMaxOracle;
	}

	public long gettPredict() {
		return tPredict;
	}

	@Override
	public void initialization(List<LabeledObject<double[], Integer>> data) {
		if (numClasses == 0) {
			// search the number of classes
			for(int i=0; i<data.size(); i++) {
				numClasses = Math.max(numClasses, data.get(i).getLabel());
			}
			numClasses++;
		}
	}

	@Override
	public double loss(Integer yTruth, Integer yPredict) {
		if (yTruth == yPredict) {
			return 0.0;
		}
		else {
			return 1.0;
		}
	}

	@Override
	public Integer maxOracle(double[] x, Integer yStar) {
		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = -1;
		double valmax = -Double.MAX_VALUE;
		for(int i=0; i<numClasses; i++) {
			Integer y = i;
			double val = loss(yStar, y) + valueOf(x, y);
			if(val>valmax){
				valmax = val;
				yPredict = y;
			}
		}

		long tEnd = System.currentTimeMillis();
		tMaxOracle += (tEnd - tStart);

		return yPredict;
	}

	@Override
	public Integer predict(double[] x) {

		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = null;
		double valmax = -Double.MAX_VALUE;
		for(int i=0; i<numClasses; i++) {
			Integer y = i;
			double val = valueOf(x, y);
			if(val>valmax){
				valmax = val;
				yPredict = y;
			}
		}

		long tEnd = System.currentTimeMillis();
		tPredict += (tEnd - tStart);

		return yPredict;
	}


	public void printTimeInformations() {
		System.out.println("Prediction=" + tPredict + " ms \t max oracle=" + tMaxOracle + " ms");
	}

	public void razTime() {
		tPredict = 0;
		tMaxOracle = 0;
	}

	public void setNumClasses(int numClasses) {
		this.numClasses = numClasses;
	}

	public void settMaxOracle(long tMaxOracle) {
		this.tMaxOracle = tMaxOracle;
	}

	public void settPredict(long tPredict) {
		this.tPredict = tPredict;
	}

	@Override
	public void sub(double[] a, double[] x, Integer y) {
		int offset = y * x.length;
		synchronized(a) {
			for(int i=0; i<x.length; i++) {
				a[offset+i] += -x[i];
			}
		}
	}

	@Override
	public String toString() {
		return "MultiClassSSVMModel [numClasses=" + numClasses + "]";
	}

	public List<Integer[]> computePrediction(List<LabeledObject<double[], Integer>> data) {
		List<Integer[]> predictionsAndLabels = new ArrayList<Integer[]>();
		for(LabeledObject<double[], Integer> example : data) {
			Integer[] p = new Integer[2];
			p[0] = predict(example.getPattern());
			p[1] = example.getLabel();
			predictionsAndLabels.add(p);
		}
		return predictionsAndLabels;
	}

	@Override
	public double valueOf(double[] x, Integer y) {
		final int offset = y * x.length;
		double[] w = getWeights();
		double score = 0;
		for(int i=0; i<x.length; i++) {
			score += x[i] * w[i+offset];
		}
		return score;
	}

}
