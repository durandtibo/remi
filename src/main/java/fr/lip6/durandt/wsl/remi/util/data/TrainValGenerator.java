package fr.lip6.durandt.wsl.remi.util.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import fr.lip6.durandt.wsl.remi.util.LabeledObject;

public class TrainValGenerator<X, Y> implements Serializable{

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 5022933308218919480L;

	private List<LabeledObject<X, Y>> train = null;

	private List<LabeledObject<X, Y>> val = null;


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	public TrainValGenerator() {
		train = null;
		val = null;
	}

	public TrainValGenerator(List<LabeledObject<X, Y>> train, List<LabeledObject<X, Y>> val) {
		this.train = train;
		this.val = val;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	public void generateTrainValSets(List<LabeledObject<X, Y>> data) {
		generateTrainValSets(data, 0.8);
	}

	public void generateTrainValSets(List<LabeledObject<X, Y>> data, double ratioTrainVal) {
		generateTrainValSets(data, ratioTrainVal, 0);
	}

	/**
	 * 
	 * @param data
	 * @param ratioValTrain pourcentage de val / train (range 0 - 1)
	 * @param randnum
	 */
	public void generateTrainValSets(List<LabeledObject<X, Y>> data, double ratioTrainVal, int seed) {

		ratioTrainVal = Math.min(Math.max(0, ratioTrainVal), 1);

		System.out.print("Generate train/val sets with a proportion of " + ratioTrainVal + " (seed=" + seed + ")  ");

		// generate the list of indices
		List<Integer> listIndex = new ArrayList<Integer>();
		for(int i=0; i<data.size(); i++) {
			listIndex.add(i);
		}

		if (seed != 0) {
			// randomize list of indices
			Random randnum = new Random(seed);
			Collections.shuffle(listIndex, randnum);
		}

		List<Integer> listIndexVal = listIndex;
		List<Integer> listIndexTrain = new ArrayList<Integer>(listIndexVal.subList(0, (int)(ratioTrainVal*data.size()))); 
		listIndexVal.removeAll(listIndexTrain);

		train = new ArrayList<LabeledObject<X, Y>>();
		for(Integer i : listIndexTrain) {
			train.add(data.get(i));
		}

		val = new ArrayList<LabeledObject<X, Y>>();
		for(Integer i : listIndexVal) {
			val.add(data.get(i));
		}

		System.out.println(this);
	}

	public List<LabeledObject<X, Y>> getTrain() {
		return train;
	}

	public int getTrainSize() {
		if(train == null) {
			return 0;
		}
		return train.size();
	}

	public List<LabeledObject<X, Y>> getVal() {
		return val;
	}

	public int getValSize() {
		if(val == null) {
			return 0;
		}
		return val.size();
	}

	public void setTrain(List<LabeledObject<X, Y>> train) {
		this.train = train;
	}

	public void setVal(List<LabeledObject<X, Y>> val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return "TrainValSet[train=" + getTrainSize() + ",val=" + getValSize() + "]";
	}

}
