package fr.lip6.durandt.wsl.remi.latent.mantra.model.io;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.lip6.durandt.wsl.remi.latent.mantra.model.MultiClassMantraModel4DoubleBag;
import fr.lip6.durandt.wsl.remi.latent.mantra.model.TopInstancesMultiClassMantraModel4DoubleBag;
import fr.lip6.durandt.wsl.remi.util.model.io.MultiClassModelIO;

public class MantraModelWriter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 889051016103792498L;

	public static void writeModelJSON(String filename, MultiClassMantraModel4DoubleBag model) {
		
		System.out.println("write model: " + filename);
		File file = new File(filename);
		file.getParentFile().mkdirs();
		
		MultiClassModelIO iomodel = new MultiClassModelIO(model.getNumClasses(), model.getWeights());
		
		ObjectMapper mapper = new ObjectMapper();
		
		//Object to JSON in file
		try {
			mapper.writeValue(file, iomodel);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void writeModelJSON(String filename, TopInstancesMultiClassMantraModel4DoubleBag model) {
		
		System.out.println("write model: " + filename);
		File file = new File(filename);
		file.getParentFile().mkdirs();
		
		MultiClassModelIO iomodel = new MultiClassModelIO(model.getNumClasses(), model.getWeights());
		
		ObjectMapper mapper = new ObjectMapper();
		
		//Object to JSON in file
		try {
			mapper.writeValue(file, iomodel);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
