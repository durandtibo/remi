package fr.lip6.durandt.wsl.remi.util.data.ranking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabeledObject;

public class RankingPattern<X> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3497149261307494023L;

	private List<X> examples;

	private List<Integer> labels;

	private int numPos = 0;

	private int numNeg = 0;


	public RankingPattern(List<LabeledObject<X, Integer>> data) {
		this(new ArrayList<X>(data.size()), new ArrayList<Integer>(data.size()));
		for(int i=0; i<data.size(); i++) {
			examples.add(i, data.get(i).getPattern());
			labels.add(i, data.get(i).getLabel());
			if(data.get(i).getLabel() == 1) {
				numPos++;
			}
			else {
				numNeg++;
			}
		}
	}

	public RankingPattern(List<X> examples, List<Integer> labels) {
		this(examples, labels, 0, 0);
		for(int i=0; i<labels.size(); i++) {
			if (labels.get(i) == 1) numPos++;
			else if (labels.get(i) == 0) numNeg++;
		}
	}

	public RankingPattern(List<X> examples, List<Integer> labels, int numPos, int numNeg) {
		super();
		this.examples = examples;
		this.labels = labels;
		this.numPos = numPos;
		this.numNeg = numNeg;
	}

	public X getExample(int i) {
		return examples.get(i);
	}
	
	public List<X> getExamples() {
		return examples;
	}

	public int getLabel(int i) {
		return labels.get(i);
	}

	public List<Integer> getLabels() {
		return labels;
	}

	public int getNumberOfExamples() {
		return examples.size();
	}

	public int getNumNeg() {
		return numNeg;
	}

	public int getNumPos() {
		return numPos;
	}

	public void setExamples(List<X> examples) {
		this.examples = examples;
	}

	public void setLabels(List<Integer> labels) {
		this.labels = labels;
	}

	public void setNumNeg(int numNeg) {
		this.numNeg = numNeg;
	}

	public void setNumPos(int numPos) {
		this.numPos = numPos;
	}

	@Override
	public String toString() {
		return "RankAPMantraModel4Bag [examples=" + examples.size() 
		+ ", labels=" + labels.size() + ", numPos=" + numPos + ", numNeg=" + numNeg + "]";
	}

}
