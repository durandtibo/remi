package fr.lip6.durandt.wsl.remi.util.solver;

import java.io.Serializable;

public abstract class SolverOptions implements Serializable {

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Builder
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	public abstract static class Builder {

		private int verbose = 1;

		private int seed = 1;


		public Builder debug(int verbose) {
			this.verbose = verbose;
			return this;
		}

		public Builder randSeed(int randSeed) {
			this.seed = randSeed;
			return this;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	protected SolverOptions(Builder builder) {
		this(builder.verbose, builder.seed);
	}

	protected SolverOptions(int verbose, int seed) {
		super();
		this.verbose = verbose;
		this.seed = seed;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 656689762171582618L;

	private int verbose;

	private int seed;

	public int getSeed() {
		return seed;
	}

	public int getVerbose() {
		return verbose;
	}

	/**
	 * print solver options
	 */
	public void print() {
		StringBuilder sb = new StringBuilder();
		sb.append("# seed=").append(seed).append("\n");
		System.out.print(sb.toString());
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}

	public void setVerbose(int verbose) {
		this.verbose = verbose;
	}

}
