package fr.lip6.durandt.wsl.remi.stuct.ssvm.model;

import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.model.PrimalStructuredModel;

public abstract class PrimalStructuralSVM<X, Y> extends PrimalStructuredModel<X, Y> implements IStructuralSVMModel<X, Y> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6065326685131742343L;

	public abstract void initialization(List<LabeledObject<X, Y>> data);

	public abstract int getDimension(X x, Y y);

}
