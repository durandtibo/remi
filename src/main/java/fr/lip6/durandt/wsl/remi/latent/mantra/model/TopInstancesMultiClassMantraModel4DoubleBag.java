package fr.lip6.durandt.wsl.remi.latent.mantra.model;

import fr.lip6.durandt.wsl.remi.util.data.bag.DoubleBag;

public class TopInstancesMultiClassMantraModel4DoubleBag extends TopInstancesMultiClassMantraModel4Bag<DoubleBag, double[]> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7039832094071629836L;

	public TopInstancesMultiClassMantraModel4DoubleBag(double k) {
		super(k);
	}

}
