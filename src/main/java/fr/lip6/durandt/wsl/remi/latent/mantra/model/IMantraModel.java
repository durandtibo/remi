package fr.lip6.durandt.wsl.remi.latent.mantra.model;

import fr.lip6.durandt.wsl.remi.latent.lssvm.model.ILatentStructuralSVMModel;

public interface IMantraModel<X, Y, H> extends ILatentStructuralSVMModel<X, Y, MantraLatent<H>> {

}
