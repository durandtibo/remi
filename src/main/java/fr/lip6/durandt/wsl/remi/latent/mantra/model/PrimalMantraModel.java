package fr.lip6.durandt.wsl.remi.latent.mantra.model;

import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.model.PrimalStructuredModel;

public abstract class PrimalMantraModel<X, Y, H> extends PrimalStructuredModel<X, LabelLatent<Y, MantraLatent<H>>> implements IMantraModel<X, Y, H> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6448608511149828199L;

	protected long tPredict = 0;

	protected long tMaxOracle = 0;


	/**
	 * a = a + psi(x,y,h)
	 * @param a
	 * @param x
	 * @param y
	 * @param h
	 */
	public void add(double[] a, X x, Y y, MantraLatent<H> h) {
		add(a, x, new LabelLatent<Y, MantraLatent<H>>(y, h));
	}

	/**
	 * a = a + gamma * psi(x,y,h)
	 * @param a
	 * @param x
	 * @param y
	 * @param h
	 * @param gamma
	 */
	public void add(double[] a, X x, Y y, MantraLatent<H> h, double gamma) {
		add(a, x, new LabelLatent<Y, MantraLatent<H>>(y, h), gamma);
	}

	@Override
	public double[] featureMap(X x, LabelLatent<Y, MantraLatent<H>> y) {
		return featureMap(x, y.getLabel(), y.getLatent());
	}

	public abstract double[] featureMap(X x, Y y, MantraLatent<H> h);

	public int getDimension(X x, LabelLatent<Y, MantraLatent<H>> y) {
		return getDimension(x, y.getLabel(), y.getLatent());
	}

	public abstract int getDimension(X x, Y y, MantraLatent<H> h);

	public double loss(LabelLatent<Y, MantraLatent<H>> yThruth, LabelLatent<Y, MantraLatent<H>> yPredict) {
		return loss(yThruth.getLabel(), yPredict.getLabel(), yPredict.getLatent());
	}

	public LabelLatent<Y, MantraLatent<H>> maxOracle(X x, LabelLatent<Y, MantraLatent<H>> y) {
		return maxOracle(x, y.getLabel());
	}

	public void printTimeInformations() {
		System.out.println("Prediction=" + tPredict + " ms \t max oracle=" + tMaxOracle + " ms");
	}

	public void razTime() {
		tPredict = 0;
		tMaxOracle = 0;
	}

	/**
	 * a = a - psi(x,y,h)
	 * @param a
	 * @param x
	 * @param y
	 * @param h
	 */
	public void sub(double[] a, X x, Y y, MantraLatent<H> h) {
		sub(a, x, new LabelLatent<Y, MantraLatent<H>>(y, h));
	}

	public double valueOf(X x, Y y, MantraLatent<H> h) {
		return valueOf(x, new LabelLatent<Y, MantraLatent<H>>(y, h));
	}

}
