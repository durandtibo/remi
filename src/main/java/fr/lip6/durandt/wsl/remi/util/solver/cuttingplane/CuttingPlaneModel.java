package fr.lip6.durandt.wsl.remi.util.solver.cuttingplane;

import java.io.Serializable;

import fr.lip6.durandt.wsl.remi.util.VectorOps;

/**
 * A cutting plane c_w'(w) is a first-order Taylor approximation computed at a particular point w'. <br/>
 * c_w'(w) = &lt a_w', w &gt + b_w' <br/>
 * 
 * @author Thibaut Durand - durand.tibo@gmail.com
 *
 */
public class CuttingPlaneModel implements Serializable{

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 4616345937646665655L;

	/**
	 * constant term of cutting plane: b_w'
	 */
	private double constant = 0.;

	/**
	 * subgradient term of cutting plane: a_w'
	 */
	private double[] subgradient = null;

	/**
	 * point of approximation: w' 
	 */
	private double[] pointOfApproximation = null;


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	public CuttingPlaneModel(double[] subgradient, double constant, double[] pointOfApproximation) {
		this.subgradient = subgradient == null ? null : subgradient.clone();
		this.constant = constant;
		this.pointOfApproximation = pointOfApproximation == null ? null : pointOfApproximation.clone();
	}

	public CuttingPlaneModel(double[] subgradient, double constant) {
		this(subgradient, constant, null);
	}

	public CuttingPlaneModel() {
		this(null, 0.0, null);
	}


	public void addCuttingPlaneModel(CuttingPlaneModel cp) {
		this.constant += cp.constant;
		VectorOps.add(this.subgradient, cp.subgradient);
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Getters and setters
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	public double getConstant() {
		return constant;
	}

	public void setConstant(double constant) {
		this.constant = constant;
	}

	public double[] getSubgradient() {
		return subgradient;
	}

	public void setSubgradient(double[] subgradient) {
		this.subgradient = subgradient;
	}

	public double[] getPointOfApproximation() {
		return pointOfApproximation;
	}

	public void setPointOfApproximation(double[] pointOfApproximation) {
		this.pointOfApproximation = pointOfApproximation;
	}

}
