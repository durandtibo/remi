package fr.lip6.durandt.wsl.remi.util.solver.bcfw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import fr.durandt.progressbar.ProgressBar;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.VectorOps;
import fr.lip6.durandt.wsl.remi.util.model.IPrimalModel;
import fr.lip6.durandt.wsl.remi.util.solver.SolverUtils;
import fr.lip6.durandt.wsl.remi.util.solver.StructuralLoss;

public class BCFWSolver<X, Y, M extends IPrimalModel> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7408592731956526252L;

	private StructuralLoss<X, Y, M> loss = null;

	private BCFWOptions options = null;

	final private double eps = 1e-8;

	public BCFWSolver(StructuralLoss<X, Y, M> loss) {
		this(loss, new BCFWOptions());
	}

	public BCFWSolver(StructuralLoss<X, Y, M> loss, BCFWOptions options) {
		super();
		this.loss = loss;
		this.options = options;
	}

	public M optimize(final M model, final List<LabeledObject<X, Y>> data) {

		// Define solver parameters
		final double lambda = options.getLambda();
		final BCFWSample mode = options.getSample();
		final int numEpochs = options.getNumEpochs();
		final boolean doWeightedAveraging = options.isDoWeightedAveraging();
		final boolean doLineSearch = options.isDoLineSearch();
		final int seed = options.getSeed();
		final int verbose = options.getVerbose();
		final int debugMultiplier = options.getDebugMultiplier();

		// initialize random generator
		final Random randnum = new Random(seed);

		// define data parameters
		// n: number of examples
		final int n = data.size(); 
		// d: dimension of the feature space
		final int d = loss.getDimension(model, data.get(0).getPattern(), data.get(0).getLabel());



		if (verbose >= 0) System.out.println("Training with " + n + " examples of dimension " + d);

		// initialize model
		if (model.getDimension() == 0) {
			// initialize the weights to zero if there are not initialized
			model.setWeights(new double[d]);
		}

		double ell = 0.0;
		double[][] wMat = new double[n][d];
		double[] ellMat = new double[n];

		// initialization in case of Weighted Averaging
		double[] wAvg = null;
		if(doWeightedAveraging) {
			wAvg = new double[d];
		}
		double lAvg = 0.0;

		// generate the list of index
		List<Integer> listIndex = new ArrayList<Integer>(n);
		for(int i=0; i<n; i++) {
			listIndex.add(i);
		}
		if(BCFWSample.PERM.equals(mode)) Collections.shuffle(listIndex, randnum);	// shuffle the list of examples

		// print information each debugIter
		int debugIter = debugMultiplier == 0 ? n : debugMultiplier;

		// initialize progress bar
		ProgressBar pb = null; 
		if (verbose == 1) {
			pb = new ProgressBar("Optimization", numEpochs, 50);
			pb.start();
		}

		int k = 0;
		for(int epoch=0; epoch<numEpochs; epoch++) {

			if(BCFWSample.PERM.equals(mode)) Collections.shuffle(listIndex, randnum);	// shuffle the list of examples

			for(Integer i : listIndex) {
				if(BCFWSample.UNIFORM.equals(mode)) i = randnum.nextInt(n); // select a random example

				// select example i
				LabeledObject<X, Y> lo = data.get(i);
				X x = lo.getPattern();
				Y y = lo.getLabel();

				// solve loss-augmented inference for point i
				Y yStar = loss.maxOracle(model, x, y);

				// define the update quantities
				double[] psii = loss.computeGradient(model, x, y, yStar);
				double lossi = loss.error(model, y, yStar);
				double ells = (1.0 / n) * lossi;
				VectorOps.mul(psii, -1.0 / (n * lambda));

				// get step-size gamma
				double gamma = 0.0;
				if (doLineSearch) {
					double[] diff = VectorOps.subn(wMat[i], psii);
					double gammaOpt = (VectorOps.dot(model.getWeights(), diff) - ((ellMat[i] - ells) * (1 / lambda))) / (VectorOps.dot(diff, diff) + eps);
					gamma = Math.max(0.0, Math.min(1.0, gammaOpt));
				}
				else {
					gamma = (2.0 * n) / (k + 2.0 * n);
				}

				// update the weights of the model
				VectorOps.sub(model.getWeights(), wMat[i]);
				VectorOps.add(wMat[i], psii, 1.0 - gamma, gamma);
				VectorOps.add(model.getWeights(), wMat[i]);

				ell = ell - ellMat[i];
				ellMat[i] = (ellMat[i] * (1.0 - gamma)) + (ells * gamma);
				ell = ell + ellMat[i];

				// update the weighted average
				if (doWeightedAveraging) {
					double rho = 2.0 / (k + 2.0);
					VectorOps.add(wAvg, model.getWeights(), (1.0 - rho), rho);
					lAvg = (lAvg * (1.0 - rho)) + (ell * rho);
				}

				k++;

				// update averaged weights
				if (doWeightedAveraging) {
					double rho = 2.0 / ((double)k + 2.0);
					VectorOps.add(wAvg, model.getWeights(), (1.0 - rho), rho);
				}

				if (verbose > 2 && k % debugIter == 0) {
					SolverUtils<X, Y, M> util = new SolverUtils<X, Y, M>();
					double primal = util.primalObjective(model, data, loss, lambda, 1.0);
					System.out.println("epoch=" + k + " \t primal=" + primal);
				}
			}
			if (verbose == 1) pb.step(); // increment the progress bar
		}
		if (verbose == 1) pb.stop(); // stop the progress bar

		if (doWeightedAveraging) {
			model.setWeights(wAvg);
		}


		return model;
	}

}
