package fr.lip6.durandt.wsl.remi.util.model.io;

import java.io.Serializable;

public class MultiClassModelIO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -315297797411779051L;

	private int numClasses;

	private double[] weights;

	public MultiClassModelIO() {
		this(0, null);
	}

	public MultiClassModelIO(int numClasses, double[] weights) {
		super();
		this.numClasses = numClasses;
		this.weights = weights;
	}

	public int getNumClasses() {
		return numClasses;
	}

	public double[] getWeights() {
		return weights;
	}

	public void setNumClasses(int numClasses) {
		this.numClasses = numClasses;
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

}
