package fr.lip6.durandt.wsl.remi.latent.lssvm.model;

import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.data.bag.DoubleBag;

public class MultiClassLSSVMModel extends PrimalLatentStructuralSVMModel<DoubleBag, Integer, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2190009011535686986L;

	private int numClasses = 0;

	private String initLatentType = "fixe";

	@Override
	public void add(double[] a, DoubleBag x, Integer y, Integer h) {
		double[] feature = x.getInstanceRepresentation(h);
		int offset = y * feature.length;
		synchronized(a) {
			for(int i=0; i<feature.length; i++) {
				a[offset+i] += feature[i];
			}
		}
	}

	@Override
	public void add(double[] a, DoubleBag x, Integer y, Integer h, double lambda) {
		double[] feature = x.getInstanceRepresentation(h);
		int offset = y * feature.length;
		synchronized(a) {
			for(int i=0; i<feature.length; i++) {
				a[offset+i] += lambda * feature[i];
			}
		}
	}

	@Override
	public double[] featureMap(DoubleBag x, Integer y, Integer h) {
		double[] feature = x.getInstanceRepresentation(h);
		double[] psi = new double[feature.length * numClasses];
		int offset = y * feature.length;
		for(int i=0; i<feature.length; i++) {
			psi[offset+i] = feature[i];
		}
		return psi;
	}

	@Override
	public int getDimension(DoubleBag x, Integer y, Integer h) {
		return x.getInstanceRepresentation(h).length * numClasses;
	}

	public String getInitLatentType() {
		return initLatentType;
	}

	public int getNumClasses() {
		return numClasses;
	}

	@Override
	public void initialization(List<LabeledObject<DoubleBag, Integer>> data) {
		if (numClasses == 0) {
			// search the number of classes
			for(int i=0; i<data.size(); i++) {
				numClasses = Math.max(numClasses, data.get(i).getLabel());
			}
			numClasses++;
		}
	}

	@Override
	public Integer initializeLatent(DoubleBag x, Integer y) {
		if (initLatentType.compareToIgnoreCase("fixe") == 0) {
			// initialize with the first element of the bag
			return 0;	
		}
		return -1;
	}

	@Override
	public double loss(Integer yTruth, Integer yPredict, Integer hPredict) {
		if (yTruth == yPredict) {
			return 0.0;
		}
		else {
			return 1.0;
		}
	}

	@Override
	public LabelLatent<Integer, Integer> maxOracle(DoubleBag x, Integer yStar) {

		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = null;
		Integer hPredict = -1;
		double valmax = -Double.MAX_VALUE;
		for(int i=0; i<numClasses; i++) {
			Integer y = i;
			for(int h=0; h<x.getNumberOfInstances(); h++) {
				double val = loss(yStar, y, h) + valueOf(x, y, h);
				if(val>valmax){
					valmax = val;
					yPredict = y;
					hPredict = h;
				}
			}
		}

		long tEnd = System.currentTimeMillis();
		tMaxOracle += (tEnd - tStart);

		return new LabelLatent<Integer, Integer>(yPredict, hPredict);
	}

	@Override
	public LabelLatent<Integer, Integer> predict(DoubleBag x) {

		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = null;
		Integer hPredict = -1;
		double valmax = -Double.MAX_VALUE;
		for(int i=0; i<numClasses; i++) {
			Integer y = i;
			for(int h=0; h<x.getNumberOfInstances(); h++) {
				double val = valueOf(x, y, h);
				if(val>valmax){
					valmax = val;
					yPredict = y;
					hPredict = h;
				}
			}
		}

		long tEnd = System.currentTimeMillis();
		tPredict += (tEnd - tStart);

		return new LabelLatent<Integer, Integer>(yPredict, hPredict);
	}

	@Override
	public Integer predict(DoubleBag x, Integer y) {
		Integer hPredict = -1;
		double valmax = -Double.MAX_VALUE;
		for(int h=0; h<x.getNumberOfInstances(); h++) {
			double val = valueOf(x, y, h);
			if(val>valmax){
				valmax = val;
				hPredict = h;
			}
		}
		return hPredict;
	}

	public void setInitLatentType(String initLatentType) {
		this.initLatentType = initLatentType;
	}

	public void setNumClasses(int numClasses) {
		this.numClasses = numClasses;
	}

	@Override
	public void sub(double[] a, DoubleBag x, Integer y, Integer h) {
		double[] feature = x.getInstanceRepresentation(h);
		int offset = y * feature.length;
		synchronized(a) {
			for(int i=0; i<feature.length; i++) {
				a[offset+i] += -feature[i];
			}
		}
	}

	@Override
	public String toString() {
		return "MultiClassLSSVMModel [numClasses=" + numClasses + ", initLatentType=" + initLatentType + "]";
	}

	@Override
	public double valueOf(DoubleBag x, Integer y, Integer h) {
		double[] feature = x.getInstance(h);
		final int offset = y * feature.length;
		double[] w = getWeights();
		double score = 0;
		for(int i=0; i<feature.length; i++) {
			score += feature[i] * w[i+offset];
		}
		return score;
	}

	public List<Integer[]> computePrediction(List<LabeledObject<DoubleBag, Integer>> data) {
		List<Integer[]> predictionsAndLabels = new ArrayList<Integer[]>();
		for(LabeledObject<DoubleBag, Integer> example : data) {
			Integer[] p = new Integer[2];
			p[0] = predict(example.getPattern()).getLabel();
			p[1] = example.getLabel();
			predictionsAndLabels.add(p);
		}
		return predictionsAndLabels;
	}
}
