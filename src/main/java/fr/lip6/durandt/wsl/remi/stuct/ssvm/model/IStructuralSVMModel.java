package fr.lip6.durandt.wsl.remi.stuct.ssvm.model;

import java.io.Serializable;

public interface IStructuralSVMModel<X, Y> extends Serializable {

	public double loss(Y yTruth, Y yPredict);
	
	public Y maxOracle(X x, Y yStar);
	
	public Y predict(X x);
	
}
