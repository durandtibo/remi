package fr.lip6.durandt.wsl.remi.util.data.ranking;

import java.util.List;

import fr.lip6.durandt.wsl.remi.util.Pair;

public class RankingAPUtil<X> {

	public RankingLabel findOptimumNegLocations(RankingPattern<X> x,
			List<Pair<Integer,Double>> positiveExamples, 
			List<Pair<Integer,Double>> negativeExamples, 
			Integer[] imgIndexMap) {

		double maxValue = 0;
		double currentValue = 0;
		int maxIndex = -1;

		Integer[] optimumLocNegImg = new Integer[x.getNumNeg()];

		// for every jth negative image
		for(int j=1; j<=x.getNumNeg(); j++){
			maxValue = 0;
			maxIndex = x.getNumPos()+1;
			// k is what we are maximising over. There would be one k_max for each negative image j
			currentValue = 0;
			for(int k=x.getNumPos(); k>=1; k--){
				currentValue += (1/(double)x.getNumPos())*((j/(double)(j+k))-((j-1)/(double)(j+k-1))) 
						- (2/(double)(x.getNumPos()*x.getNumNeg()))*(positiveExamples.get(k-1).getValue() - negativeExamples.get(j-1).getValue());
				if(currentValue > maxValue){
					maxValue = currentValue;
					maxIndex = k;
				}
			}
			optimumLocNegImg[j-1] = maxIndex;
		}

		return encodeRanking(x, positiveExamples, negativeExamples, imgIndexMap, optimumLocNegImg);
	}

	private RankingLabel encodeRanking(RankingPattern<X> x, 
			List<Pair<Integer,Double>> positiveExamples, 
			List<Pair<Integer,Double>> negativeExamples, 
			Integer[] imgIndexMap, 
			Integer[] optimumLocNegImg){

		Integer[] ranking = new Integer[x.getNumberOfExamples()];
		for(int i=0; i<ranking.length; i++) {
			ranking[i] = 0;
		}

		for(int i=0; i<x.getNumberOfExamples(); i++){
			for(int j=i+1; j<x.getNumberOfExamples(); j++){        
				if(i == j){
					// Nothing to do
				}
				else if(x.getLabel(i) == x.getLabel(j)){                
					if(x.getLabel(i) == 1){                                 
						if(positiveExamples.get(imgIndexMap[i]).getValue() > positiveExamples.get(imgIndexMap[j]).getValue()){
							ranking[i]++;
							ranking[j]--;
						}
						else if(positiveExamples.get(imgIndexMap[j]).getValue() > positiveExamples.get(imgIndexMap[i]).getValue()){
							ranking[i]--;
							ranking[j]++;
						}
						else{
							if(i < j){
								ranking[i]++;
								ranking[j]--;
							}
							else{
								ranking[i]--;
								ranking[j]++;
							}
						}
					}
					else{
						if(negativeExamples.get(imgIndexMap[i]).getValue() > negativeExamples.get(imgIndexMap[j]).getValue()){
							ranking[i]++;
							ranking[j]--;
						}
						else if(negativeExamples.get(imgIndexMap[j]).getValue() > negativeExamples.get(imgIndexMap[i]).getValue()){
							ranking[i]--;
							ranking[j]++;
						}
						else{
							if(i < j){
								ranking[i]++;
								ranking[j]--;
							}
							else{
								ranking[i]--;
								ranking[j]++;
							}
						}
					}        
				}
				else if((x.getLabel(i) == 1) && (x.getLabel(j) == 0)){
					int iPrime = imgIndexMap[i]+1;
					int jPrime = imgIndexMap[j]+1;
					int ojPrime = optimumLocNegImg[jPrime-1];

					if((ojPrime - iPrime - 0.5) > 0){
						ranking[i]++;
						ranking[j]--;
					}
					else{
						ranking[i]--;
						ranking[j]++;
					}
				}
				else if((x.getLabel(i) == 0) && (x.getLabel(j) == 1)){
					int iPrime = imgIndexMap[i]+1;
					int jPrime = imgIndexMap[j]+1;
					int oiPrime = optimumLocNegImg[iPrime-1];

					if((jPrime - oiPrime + 0.5) > 0){
						ranking[i]++;
						ranking[j]--;
					}
					else{
						ranking[i]--;
						ranking[j]++;
					}
				}                    
			}        
		}    

		return new RankingLabel(ranking, x.getNumPos(), x.getNumNeg());
	}
	
	public static double averagePrecision(RankingLabel yTruth, RankingLabel yPredict) {

		int numExamples = yTruth.getNumberOfExamples();

		// Stores rank of all images
		int[] ranking = new int[numExamples]; 
		// Stores list of images sorted by rank. Higher rank to lower rank 
		int[] sortedExamples = new int[numExamples]; 

		// convert rank matrix to rank list
		for(int i=0; i<numExamples; i++){
			// start with lowest rank for each sample i.e 1 
			ranking[i] = 1; 
			for(int j=0; j<numExamples; j++){
				if(yPredict.getRanking(i) > yPredict.getRanking(j)){
					ranking[i] = ranking[i] + 1;
				} 
			}
			sortedExamples[numExamples - ranking[i]] = i;
		}  

		int posCount = 0;
		int totalCount = 0;
		double precisionAti = 0;
		for(int i=0; i<numExamples; i++){
			int label = yTruth.getLabel(sortedExamples[i]);
			if(label == 1){
				posCount++;
				totalCount++;
			}
			else{
				totalCount++;
			}
			if(label == 1){
				precisionAti = precisionAti + (double)posCount/(double)totalCount;
			}
		}
		precisionAti = precisionAti/(double)posCount;

		return precisionAti;
	}

}

