package fr.lip6.durandt.wsl.remi.util.data;

import java.util.Collections;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.Pair;

public class Evaluation {

	public static double multiClassAccuracy(List<Integer[]> predictionsAndLabels) {
		int numCorrects = 0;
		for(Integer[] p : predictionsAndLabels) {
			if (p[0] == p[1]) numCorrects++;
		}
		double accuracy = (double)numCorrects / (double)predictionsAndLabels.size();
		System.out.println("accuracy=" + accuracy + "\t number of correct predictions=" + numCorrects);
		return accuracy;
	}

	public static double f1Score(List<Integer[]> predictionsAndLabels) {
		return fScore(predictionsAndLabels, 1);
	}

	public static double fScore(List<Integer[]> predictionsAndLabels, double beta) {
		double tp = 0;
		double fn = 0;
		double fp = 0;
		for(Integer[] p : predictionsAndLabels) {
			Integer prediction = p[0];
			Integer label = p[1];
			if (label == 1 && prediction == 1) tp++;
			if (label == 1 && prediction <= 0) fn++;
			if (label <= 0 && prediction == 1) fp++;
		}
		double p = tp / (tp + fp);
		double r = tp / (tp + fn);
		double f = (1 + beta * beta) * (p * r) / (beta * beta * p + r);
		if (Double.isNaN(f)) f = 0;
		System.out.println("fscore=" + f + "\ttp=" + tp + "\tfn=" + fn + "\tfp=" + fp + "\tp=" + p + "\tr=" + r);
		return f;
	}
	
	public static double averagePrecision(List<Pair<Integer, Double>> scoresAndLabels) {

		if (scoresAndLabels == null) return 0.0;

		// Sort the examples per decreasing scores 
		Collections.sort(scoresAndLabels, Collections.reverseOrder());

		int[] tp = new int[scoresAndLabels.size()];
		int[] fp = new int[scoresAndLabels.size()];

		int i = 0;
		int cumtp = 0, cumfp = 0;
		int totalpos = 0;

		//cumsum of true positives and false positives
		for(Pair<Integer, Double> e : scoresAndLabels) {
			if(e.getKey() == 1) {
				cumtp++;
				totalpos++;
			}
			else {
				cumfp++;
			}
			tp[i] = cumtp;
			fp[i] = cumfp;
			i++;
		}

		//precision / recall
		double[] prec = new double[tp.length];
		double[] reca = new double[tp.length];

		for(i = 0 ; i < tp.length ; i++) {
			reca[i] = ((double)tp[i])/((double)totalpos);
			prec[i] = ((double)tp[i])/((double)(tp[i]+fp[i]));
		}

		double[] mrec = new double[reca.length+2];
		for(int j=0; j<reca.length; j++) {
			mrec[j+1] = reca[j];
		}
		mrec[mrec.length-1] = 1;

		double[] mpre = new double[prec.length+2];
		for(int j=0; j<prec.length; j++) {
			mpre[j+1] = prec[j];
		}

		for(int j=mpre.length-2; j>=0; j--) {
			mpre[j] = Math.max(mpre[j],mpre[j+1]);
		}

		double map = 0.;
		for(int j=1; j<mpre.length-1; j++) {
			map += (mrec[j]-mrec[j-1])*mpre[j];
		}

		return map;

	}
}
