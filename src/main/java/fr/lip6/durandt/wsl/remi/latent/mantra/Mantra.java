package fr.lip6.durandt.wsl.remi.latent.mantra;

import java.io.Serializable;
import java.util.List;

import fr.lip6.durandt.wsl.remi.latent.mantra.model.PrimalMantraModel;
import fr.lip6.durandt.wsl.remi.latent.mantra.solver.MantraSolver;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;

public class Mantra<X, Y, H> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7845506801188535024L;

	private MantraSolver<X, Y, H> solver;

	public Mantra(MantraSolver<X, Y, H> solver) {
		super();
		this.solver = solver;
	}

	public PrimalMantraModel<X, Y, H> train(
			PrimalMantraModel<X, Y, H> model,
			List<LabeledObject<X, Y>> data,
			List<LabeledObject<X, Y>> testData) {

		final int verbose = solver.getOptions().getVerbose();

		// display some informations
		if (verbose >= 0) {
			System.out.println("\n----------------------------------------------------------------------------------------------------");
			System.out.println("############################################# MANTRA ###############################################");
			solver.getOptions().print();
			System.out.println("----------------------------------------------------------------------------------------------------");
			model.initialization(data);
			System.out.println("# " + model);
			System.out.println("----------------------------------------------------------------------------------------------------");
		}

		// optimization
		long startTime = System.currentTimeMillis();
		PrimalMantraModel<X, Y, H> leranedModel = solver.optimize(model, data);
		long endTime = System.currentTimeMillis();

		if (verbose >= 0) {
			System.out.println("End optimization - Time= " + (endTime - startTime) + " ms");
			// evaluate learned model
			solver.evaluate(leranedModel, data, testData);
			System.out.println("----------------------------------------------------------------------------------------------------");
		}

		return leranedModel;
	}

}
