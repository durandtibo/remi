package fr.lip6.durandt.wsl.remi.latent.mantra.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.Pair;
import fr.lip6.durandt.wsl.remi.util.VectorOps;
import fr.lip6.durandt.wsl.remi.util.data.bag.Bag;
import fr.lip6.durandt.wsl.remi.util.data.ranking.RankingAPUtil;
import fr.lip6.durandt.wsl.remi.util.data.ranking.RankingLabel;
import fr.lip6.durandt.wsl.remi.util.data.ranking.RankingPattern;

public class RankAPMantraModel4Bag<X extends Bag<Z>, Z> extends PrimalMantraModel<RankingPattern<X>, RankingLabel, List<Integer>> {

	private String initLatentType = "fixe";

	public long tFeatureMap = 0;
	
	public long tMaxOracle = 0;

	/**
	 * 
	 */
	private static final long serialVersionUID = -3738039290039441526L;

	@Override
	public double[] featureMap(RankingPattern<X> x, RankingLabel y, MantraLatent<List<Integer>> h) {

		long t0 = System.currentTimeMillis();

		// use the first example to define the dimension of the feature map
		int dim = x.getExample(0).getInstanceRepresentation(0).length;
		double[] psi = new double[dim];

		int[] count = new int[x.getNumberOfExamples()];
		for(int i=0; i<x.getNumberOfExamples(); i++) {
			if(x.getLabel(i) == 1) {
				for(int j=0; j<x.getNumberOfExamples(); j++) {
					if(x.getLabel(j) == 0) {
						int yij = -1;
						if(y.getRanking(i) > y.getRanking(j)){
							yij = 1;
						}
						if(yij == 1) { // Yij = +1
							count[i]++;
							count[j]--;
						}
						else {	// Yij = -1
							count[i]--;
							count[j]++;
						}
					}
				}
			}
		}

		for(int i=0; i<x.getNumberOfExamples(); i++) {
			double[] psiMax = x.getExample(i).getInstanceRepresentation(h.gethMax().get(i));
			double[] psiMin = x.getExample(i).getInstanceRepresentation(h.gethMin().get(i));
			for(int d=0; d<dim; d++) {
				psi[d] += (psiMax[d] + psiMin[d]) * count[i];
			}
		}

		// Divide by the number of relevant examples * number of irrelevant examples
		double c = (double)(x.getNumPos()*x.getNumNeg());
		for(int d=0; d<dim; d++) {
			psi[d] /= c;
		}

		long t1 = System.currentTimeMillis();
		tFeatureMap += (t1 - t0);

		return psi;
	}

	@Override
	public int getDimension(RankingPattern<X> x, RankingLabel y, MantraLatent<List<Integer>> h) {
		return x.getExample(0).getInstanceRepresentation(0).length;
	}

	@Override
	public void initialization(List<LabeledObject<RankingPattern<X>, RankingLabel>> data) {

		RankingPattern<X> pattern = data.get(0).getPattern();
		for(int i=0; i<pattern.getNumberOfExamples(); i++) {
			if (pattern.getLabel(i) != 1 && pattern.getLabel(i) != 0) {
				throw new IllegalArgumentException("Incorrect label: " + pattern.getLabel(i) + "\texpected (1 and 0)");
			}
		}

	}

	@Override
	public MantraLatent<List<Integer>> initializeLatent(RankingPattern<X> x, RankingLabel y) {
		List<Integer> hPredictMax = new ArrayList<Integer>();
		List<Integer> hPredictMin = new ArrayList<Integer>();

		if (initLatentType.compareToIgnoreCase("fixe") == 0) {
			// compute the score of each example
			for(int i=0; i<x.getNumberOfExamples(); i++) {
				hPredictMax.add(i, 0);
				hPredictMin.add(i, 0);
			}
		}
		else {
			throw new IllegalArgumentException("Incorrect initialisation: " + initLatentType);
		}

		MantraLatent<List<Integer>> hPredict = new MantraLatent<List<Integer>>(hPredictMax, hPredictMin);
		return hPredict;
	}

	@Override
	public double loss(RankingLabel yTruth, RankingLabel yPredict, MantraLatent<List<Integer>> hPredict) {
		return 1 - RankingAPUtil.averagePrecision(yTruth, yPredict);
	}

	@Override
	public LabelLatent<RankingLabel, MantraLatent<List<Integer>>> maxOracle(RankingPattern<X> x, RankingLabel yStar) {
		
		long t0 = System.currentTimeMillis();
		
		List<Pair<Integer,Double>> positiveExamples = new ArrayList<Pair<Integer,Double>>(x.getNumPos());
		List<Pair<Integer,Double>> negativeExamples = new ArrayList<Pair<Integer,Double>>(x.getNumNeg());

		List<Integer> hPredictMax = new ArrayList<Integer>();
		List<Integer> hPredictMin = new ArrayList<Integer>();

		// compute the score of each example
		for(int i=0; i<x.getNumberOfExamples(); i++) {
			X example = x.getExample(i);
			MantraLatent<Integer> hPredict = predict(example);
			Integer hMax = hPredict.gethMax();
			Integer hMin = hPredict.gethMin();
			double score = valueOf(example, hMax) + valueOf(example, hMin);

			if(x.getLabel(i) == 1) {
				positiveExamples.add(new Pair<Integer, Double>(i, score));
			}
			else {
				negativeExamples.add(new Pair<Integer, Double>(i, score));
			}

			hPredictMax.add(i, hMax);
			hPredictMin.add(i, hMin);
		}

		MantraLatent<List<Integer>> hPredict = new MantraLatent<List<Integer>>(hPredictMax, hPredictMin);

		// sort positiveExamples and negativeExamples in descending order of score
		Collections.sort(positiveExamples, Collections.reverseOrder());
		Collections.sort(negativeExamples, Collections.reverseOrder());

		int negativeId = 0;
		int positiveId = 0; 
		Integer[] imgIndexMap = new Integer[x.getNumberOfExamples()];
		for(int i=0; i<x.getNumberOfExamples(); i++){
			if(x.getLabel(i) == 1){
				imgIndexMap[positiveExamples.get(positiveId).getKey()] = positiveId;
				positiveId++;
			}
			else{
				imgIndexMap[negativeExamples.get(negativeId).getKey()] = negativeId;
				negativeId++;
			}
		}    

		RankingAPUtil<X> util = new RankingAPUtil<X>();
		RankingLabel yPredict = util.findOptimumNegLocations(x, positiveExamples, negativeExamples, imgIndexMap);
		
		long t1 = System.currentTimeMillis();
		tMaxOracle += (t1 - t0);

		return new LabelLatent<RankingLabel, MantraLatent<List<Integer>>>(yPredict, hPredict);
	}

	@Override
	public LabelLatent<RankingLabel, MantraLatent<List<Integer>>> predict(RankingPattern<X> x) {

		List<Integer> hPredictMax = new ArrayList<Integer>();
		List<Integer> hPredictMin = new ArrayList<Integer>();

		List<Pair<Integer,Double>> examples = new ArrayList<Pair<Integer,Double>>(x.getNumberOfExamples());

		// compute the score of each example
		for(int i=0; i<x.getNumberOfExamples(); i++) {
			X example = x.getExample(i);
			// latent prediction
			MantraLatent<Integer> hPredict = predict(example);
			Integer hMax = hPredict.gethMax();
			Integer hMin = hPredict.gethMin();
			// score = max + min
			double score = valueOf(example, hMax) + valueOf(example, hMin);

			examples.add(new Pair<Integer, Double>(i, score));

			hPredictMax.add(i, hMax);
			hPredictMin.add(i, hMin);
		}

		MantraLatent<List<Integer>> hPredict = new MantraLatent<List<Integer>>(hPredictMax, hPredictMin);

		// sort examples in descending order of score
		Collections.sort(examples, Collections.reverseOrder());

		// compute the ranking
		Integer[] ranking = new Integer[examples.size()];
		for(int i=0; i<examples.size(); i++) {
			ranking[examples.get(i).getKey()] = x.getNumberOfExamples() - i;
		}

		RankingLabel yPredict = new RankingLabel(ranking, x.getNumPos(), x.getNumNeg());

		return new LabelLatent<RankingLabel, MantraLatent<List<Integer>>>(yPredict, hPredict);
	}

	@Override
	public MantraLatent<List<Integer>> predict(RankingPattern<X> x, RankingLabel y) {

		List<Integer> hPredictMax = new ArrayList<Integer>();
		List<Integer> hPredictMin = new ArrayList<Integer>();

		// compute the score of each example
		for(int i=0; i<x.getNumberOfExamples(); i++) {
			X example = x.getExample(i);

			// latent prediction
			MantraLatent<Integer> hPredict = predict(example);
			Integer hMax = hPredict.gethMax();
			Integer hMin = hPredict.gethMin();

			hPredictMax.add(i, hMax);
			hPredictMin.add(i, hMin);
		}

		MantraLatent<List<Integer>> hPredict = new MantraLatent<List<Integer>>(hPredictMax, hPredictMin);
		return hPredict;
	}

	protected MantraLatent<Integer> predict(X x) {

		Integer hMax = null;
		Integer hMin = null;
		double valmax = -Double.MAX_VALUE;
		double valmin = Double.MAX_VALUE;

		for(int h=0; h<x.getNumberOfInstances(); h++) {
			double val = valueOf(x, h);
			if(val>valmax){
				valmax = val;
				hMax = h;
			}
			if(val<valmin){
				valmin = val;
				hMin = h;
			}
		}

		return new MantraLatent<Integer>(hMax, hMin);
	}

	protected double valueOf(X x, Integer h) {
		double[] feature = x.getInstanceRepresentation(h);
		double[] w = getWeights();
		return VectorOps.dot(w, feature);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [initLatentType=" + initLatentType + "]";
	}


}
