package fr.lip6.durandt.wsl.remi.latent.mantra.solver.cuttingplane;

import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.remi.latent.mantra.model.MantraLatent;
import fr.lip6.durandt.wsl.remi.latent.mantra.model.PrimalMantraModel;
import fr.lip6.durandt.wsl.remi.latent.mantra.solver.MantraLoss2;
import fr.lip6.durandt.wsl.remi.latent.mantra.solver.MantraSolver;
import fr.lip6.durandt.wsl.remi.latent.mantra.solver.MantraUtils;
import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.solver.SolverOptions;
import fr.lip6.durandt.wsl.remi.util.solver.cuttingplane.OneSCPOptions;
import fr.lip6.durandt.wsl.remi.util.solver.cuttingplane.OneSCPSolver;

public class MantraWith1SCP<X, Y, H> extends MantraSolver<X, Y, H> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6807195219515638221L;

	private OneSCPOptions options = null;

	public MantraWith1SCP(OneSCPOptions options) {
		super();
		this.options = options;
	}

	public MantraWith1SCP() {
		this(new OneSCPOptions());
	}

	@Override
	public SolverOptions getOptions() {
		return options;
	}

	@Override
	public PrimalMantraModel<X, Y, H> optimize(final PrimalMantraModel<X, Y, H> model, final List<LabeledObject<X, Y>> data) {

		List<LabeledObject<X, LabelLatent<Y, MantraLatent<H>>>> newData = new ArrayList<LabeledObject<X, LabelLatent<Y, MantraLatent<H>>>>(data.size());
		for(LabeledObject<X, Y> example : data) {
			LabelLatent<Y, MantraLatent<H>> newLabel = new LabelLatent<Y, MantraLatent<H>>(example.getLabel(), model.initializeLatent(example.getPattern(), example.getLabel()));
			newData.add(new LabeledObject<X, LabelLatent<Y, MantraLatent<H>>>(example.getPattern(), newLabel));
		}

		OneSCPSolver<X, LabelLatent<Y, MantraLatent<H>>, PrimalMantraModel<X, Y, H>> solver = new OneSCPSolver<X, LabelLatent<Y, MantraLatent<H>>, PrimalMantraModel<X, Y, H>>(new MantraLoss2<X, Y, H>(), options);
		solver.optimize(model, newData);
		return model;
	}

	@Override
	public double primalObjective(final PrimalMantraModel<X, Y, H> model, 
			final List<LabeledObject<X, Y>> data) {
		MantraUtils<X, Y, H> util = new MantraUtils<X, Y, H>(options.getnThreads());
		return util.primalObjective(model, data, 1.0, options.getC());
	}

}
