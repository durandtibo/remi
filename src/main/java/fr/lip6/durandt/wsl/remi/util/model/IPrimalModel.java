package fr.lip6.durandt.wsl.remi.util.model;

import fr.lip6.durandt.wsl.remi.util.VectorOps;

public interface IPrimalModel {

	public double[] getWeights();

	public int getDimension();
	
	/**
	 * @return ||w||^2
	 */
	public default double norm2() {
		return VectorOps.dot(getWeights(), getWeights());
	}

	public void setWeights(double[] weights);
	
}
