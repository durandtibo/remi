package fr.lip6.durandt.wsl.remi.latent.lssvm.model;

import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.model.PrimalStructuredModel;

public abstract class PrimalLatentStructuralSVMModel<X, Y, H> extends PrimalStructuredModel<X, LabelLatent<Y, H>> implements ILatentStructuralSVMModel<X, Y, H> {

	protected long tPredict = 0;

	protected long tMaxOracle = 0;

	/**
	 * 
	 */
	private static final long serialVersionUID = 6065326685131742343L;

	/**
	 * a = a + psi(x,y,h)
	 * @param a
	 * @param x
	 * @param y
	 * @param h
	 */
	public void add(double[] a, X x, Y y, H h) {
		add(a, x, new LabelLatent<Y, H>(y, h));
	}

	/**
	 * a = a + gamma * psi(x,y,h)
	 * @param a
	 * @param x
	 * @param y
	 * @param h
	 * @param gamma
	 */
	public void add(double[] a, X x, Y y, H h, double gamma) {
		add(a, x, new LabelLatent<Y, H>(y, h), gamma);
	}

	@Override
	public double[] featureMap(X x, LabelLatent<Y, H> y) {
		return featureMap(x, y.getLabel(), y.getLatent());
	}

	public LabelLatent<Y, H> maxOracle(X x, LabelLatent<Y, H> y) {
		return maxOracle(x, y.getLabel());
	}

	public abstract double[] featureMap(X x, Y y, H h);

	public double valueOf(X x, Y y, H h) {
		return valueOf(x, new LabelLatent<Y, H>(y, h));
	}

	public double loss(LabelLatent<Y, H> yThruth, LabelLatent<Y, H> yPredict) {
		return loss(yThruth.getLabel(), yPredict.getLabel(), yPredict.getLatent());
	}

	public abstract int getDimension(X x, Y y, H h);

	public int getDimension(X x, LabelLatent<Y, H> y) {
		return getDimension(x, y.getLabel(), y.getLatent());
	}

	/**
	 * a = a - psi(x,y,h)
	 * @param a
	 * @param x
	 * @param y
	 * @param h
	 */
	public void sub(double[] a, X x, Y y, H h) {
		sub(a, x, new LabelLatent<Y, H>(y, h));
	}

	public void printTimeInformations() {
		System.out.println("Prediction=" + tPredict + " ms \t max oracle=" + tMaxOracle + " ms");
	}

	public void razTime() {
		tPredict = 0;
		tMaxOracle = 0;
	}

}
