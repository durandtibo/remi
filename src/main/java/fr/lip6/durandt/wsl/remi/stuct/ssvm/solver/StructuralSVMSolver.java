package fr.lip6.durandt.wsl.remi.stuct.ssvm.solver;

import java.io.Serializable;
import java.util.List;

import fr.lip6.durandt.wsl.remi.stuct.ssvm.model.PrimalStructuralSVM;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.solver.SolverOptions;

public abstract class StructuralSVMSolver<X, Y> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2905790934823410431L;

	public void evaluate(final PrimalStructuralSVM<X, Y> model,
			final List<LabeledObject<X, Y>> data,
			final List<LabeledObject<X, Y>> testData) {

		double primal = primalObjective(model, data);
		StructSVMUtils<X, Y> util = new StructSVMUtils<X, Y>();
		double trainError = util.empiricalLoss(model, data);

		if (testData != null) {
			double testError = util.empiricalLoss(model, testData);
			System.out.println("Primal objective= " + primal + ", Train error= " + trainError + ", Test error= " + testError);
		} else {
			System.out.println("Primal objective= " + primal + ", Train error= " + trainError);
		}
	}

	public abstract SolverOptions getOptions();

	public abstract PrimalStructuralSVM<X, Y> optimize(final PrimalStructuralSVM<X, Y> model,
			final List<LabeledObject<X, Y>> data);
	
	public abstract double primalObjective(final PrimalStructuralSVM<X, Y> model,
			final List<LabeledObject<X, Y>> data);

}
