package fr.lip6.durandt.wsl.remi.util.data.ranking;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class RankingLabel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7844845229975941520L;

	private Integer[] ranking;

	private Integer[] labels;

	private int numPos;

	private int numNeg;

	public RankingLabel(Integer[] ranking, int numPos, int numNeg) {
		this(ranking == null ? null : ranking.clone(), null, numPos, numNeg);
	}

	public RankingLabel(Integer[] ranking, Integer[] labels, int numPos, int numNeg) {
		super();
		this.ranking = ranking;
		this.labels = labels;
		this.numPos = numPos;
		this.numNeg = numNeg;
	}

	public RankingLabel(List<Integer> labelsGT) {

		this(new Integer[labelsGT.size()], new Integer[labelsGT.size()], 0, 0);

		// Initialize labels
		for(int i=0; i<labelsGT.size(); i++) {
			labels[i] = labelsGT.get(i);
			if(labelsGT.get(i) == 1) {
				numPos++;
			}
			else {
				numNeg++;
			}
		}

		// Initialize ranking
		for(int i=0; i<ranking.length; i++) {
			ranking[i] = 0;
		}

		// Compute ranking 
		for(int i=0; i<labels.length; i++) {
			for(int j=i+1; j<labels.length; j++){
				if(labels[i] == 1){
					if(labels[j] != 0){
						ranking[i]++;
						ranking[j]--;
					}              
				}
				else{
					if(labels[j] == 1){
						ranking[i]--;
						ranking[j]++;
					}
				}
			}
		}
	}

	public Integer[] getLabels() {
		return labels;
	}

	public int getNumNeg() {
		return numNeg;
	}

	public int getNumPos() {
		return numPos;
	}

	public Integer[] getRanking() {
		return ranking;
	}

	public int getRanking(int i) {
		return ranking[i];
	}

	public void setLabels(Integer[] labels) {
		this.labels = labels;
	}

	public void setNumNeg(int numNeg) {
		this.numNeg = numNeg;
	}

	public void setNumPos(int numPos) {
		this.numPos = numPos;
	}

	public void setRanking(Integer[] ranking) {
		this.ranking = ranking;
	}

	@Override
	public String toString() {
		return "RankingLabel [ranking=" + Arrays.toString(ranking) + ", labels=" + Arrays.toString(labels) 
		+ ", numPos=" + numPos + ", numNeg=" + numNeg + "]";
	}

	public int getNumberOfExamples() {
		return labels.length;
	}

	public int getLabel(int i) {
		return labels[i];
	}

}
