package fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.bcfw;

import java.util.List;

import fr.lip6.durandt.wsl.remi.stuct.ssvm.model.PrimalStructuralSVM;
import fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.StructSVMUtils;
import fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.StructuralSVMLoss;
import fr.lip6.durandt.wsl.remi.stuct.ssvm.solver.StructuralSVMSolver;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.solver.SolverOptions;
import fr.lip6.durandt.wsl.remi.util.solver.bcfw.BCFWOptions;
import fr.lip6.durandt.wsl.remi.util.solver.bcfw.BCFWSolver;

public class StructuralSVMWithBCFW<X, Y> extends StructuralSVMSolver<X, Y> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5051909035685423481L;

	private BCFWOptions options = null;

	public StructuralSVMWithBCFW(BCFWOptions options) {
		super();
		this.options = options;
	}

	public StructuralSVMWithBCFW() {
		this(new BCFWOptions());
	}

	@Override
	public SolverOptions getOptions() {
		return options;
	}

	@Override
	public PrimalStructuralSVM<X, Y> optimize(final PrimalStructuralSVM<X, Y> model, final List<LabeledObject<X, Y>> data) {
		BCFWSolver<X, Y, PrimalStructuralSVM<X, Y>> solver = new BCFWSolver<X, Y, PrimalStructuralSVM<X, Y>>(new StructuralSVMLoss<X, Y>(), options);
		solver.optimize(model, data);
		return model;
	}

	@Override
	public double primalObjective(PrimalStructuralSVM<X, Y> model, List<LabeledObject<X, Y>> data) {
		StructSVMUtils<X, Y> util = new StructSVMUtils<X, Y>();
		return util.primalObjective(model, data, options.getLambda(), 1.0);
	}

}
