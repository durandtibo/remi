package fr.lip6.durandt.wsl.remi.util.solver.cuttingplane;

import fr.lip6.durandt.wsl.remi.util.solver.SolverOptions;

public class OneSCPOptions extends SolverOptions {

	public static class Builder extends SolverOptions.Builder {

		/**
		 * regularization parameter
		 */
		private double c = 1e4;

		/**
		 * Maximum number of cutting-plane models
		 */
		private int maxCuttingPlanes = 500;

		/**
		 * Minimum number of cutting-plane models
		 */
		private int minCuttingPlanes = 5;

		/**
		 * Precision
		 */
		private double epsilon = 1e-2;

		private int nThreads = 1;

		/**
		 * Number of threads for Mosek solver
		 */
		private int nThreadsMosek = 1;

		private int debugMultiplier = 1;


		public OneSCPOptions build() {
			return new OneSCPOptions(this);
		}

		public Builder c(double c) {
			this.c = c;
			return this;
		}


		public Builder debugMultiplier(int debugMultiplier) {
			this.debugMultiplier = debugMultiplier;
			return this;
		}

		public Builder epsilon(double epsilon) {
			this.epsilon = epsilon;
			return this;
		}

		public Builder maxCuttingPlanes(int maxCuttingPlanes) {
			this.maxCuttingPlanes = maxCuttingPlanes;
			return this;
		}

		public Builder minCuttingPlanes(int minCuttingPlanes) {
			this.minCuttingPlanes = minCuttingPlanes;
			return this;
		}

		public Builder nThreads(int nThreads) {
			this.nThreads = nThreads;
			return this;
		}

		public Builder nThreadsMosek(int nThreadsMosek) {
			this.nThreadsMosek = nThreadsMosek;
			return this;
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Builder
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 8615146150624388539L;

	/**
	 * regularization parameter
	 */
	private double c;

	/**
	 * Maximum number of cutting-plane models
	 */
	private int maxCuttingPlanes;

	/**
	 * Minimum number of cutting-plane models
	 */
	private int minCuttingPlanes;

	/**
	 * Precision
	 */
	private double epsilon;

	private int nThreads;

	/**
	 * Number of threads for Mosek solver
	 */
	private int nThreadsMosek;

	private int debugMultiplier;


	public OneSCPOptions() {
		this(new OneSCPOptions.Builder());
	}

	protected OneSCPOptions(Builder builder) {
		super(builder);
		this.c = builder.c;
		this.maxCuttingPlanes = builder.maxCuttingPlanes;
		this.minCuttingPlanes = builder.minCuttingPlanes;
		this.epsilon = builder.epsilon;
		this.nThreads = builder.nThreads;
		this.nThreadsMosek = builder.nThreadsMosek;
		this.debugMultiplier = builder.debugMultiplier;
	}

	public OneSCPOptions(
			double c,
			int maxCuttingPlanes, 
			int minCuttingPlanes, 
			double epsilon, 
			int nThreads, 
			int nThreadsMosek, 
			int debugMultiplier,
			int verbose,
			int seed) {

		super(verbose, seed);
		this.c = c;
		this.maxCuttingPlanes = maxCuttingPlanes;
		this.minCuttingPlanes = minCuttingPlanes;
		this.epsilon = epsilon;
		this.nThreads = nThreads;
		this.nThreadsMosek = nThreadsMosek;
		this.debugMultiplier = debugMultiplier;
	}

	public double getC() {
		return c;
	}

	public int getDebugMultiplier() {
		return debugMultiplier;
	}

	public double getEpsilon() {
		return epsilon;
	}

	public int getMaxCuttingPlanes() {
		return maxCuttingPlanes;
	}

	public int getMinCuttingPlanes() {
		return minCuttingPlanes;
	}

	public int getnThreads() {
		return nThreads;
	}

	public int getnThreadsMosek() {
		return nThreadsMosek;
	}

	public void print() {

		StringBuilder sb = new StringBuilder();
		sb.append("# 1 Slack Cutting-Plane options\n");
		sb.append("# C=").append(c).append("\n");
		sb.append("# maxCuttingPlanes=").append(maxCuttingPlanes).append("\n");
		sb.append("# minCuttingPlanes=").append(minCuttingPlanes).append("\n");
		sb.append("# epsilon=").append(epsilon).append("\n");
		sb.append("# nThreads=").append(nThreads).append("\n");
		sb.append("# nThreadsMosek=").append(nThreadsMosek).append("\n");
		if (getVerbose() > 1) sb.append("# debugMultiplier=").append(debugMultiplier).append("\n");
		System.out.print(sb.toString());
		super.print();
	}

	public void setC(double c) {
		this.c = c;
	}

	public void setDebugMultiplier(int debugMultiplier) {
		this.debugMultiplier = Math.max(1, debugMultiplier);
	}

	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}

	public void setMaxCuttingPlanes(int maxCuttingPlanes) {
		this.maxCuttingPlanes = maxCuttingPlanes;
	}

	public void setMinCuttingPlanes(int minCuttingPlanes) {
		this.minCuttingPlanes = minCuttingPlanes;
	}

	public void setnThreads(int nThreads) {
		this.nThreads = nThreads;
	}

	public void setnThreadsMosek(int nThreadsMosek) {
		this.nThreadsMosek = nThreadsMosek;
	}

	@Override
	public String toString() {
		return "OneSCPOptions [c=" + c + 
				", maxCuttingPlanes=" + maxCuttingPlanes + 
				", minCuttingPlanes=" + minCuttingPlanes + 
				", epsilon=" + epsilon + 
				", nThreads=" + nThreads + 
				", nThreadsMosek=" + nThreadsMosek + 
				", debugMultiplier=" + debugMultiplier + "]";
	}


}
