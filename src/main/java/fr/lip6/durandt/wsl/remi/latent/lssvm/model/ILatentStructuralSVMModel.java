package fr.lip6.durandt.wsl.remi.latent.lssvm.model;

import java.io.Serializable;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;

public interface ILatentStructuralSVMModel<X, Y, H> extends Serializable {

	public double loss(Y yTruth, Y yPredict, H hPredict);

	public LabelLatent<Y, H> maxOracle(X x, Y yStar);

	public LabelLatent<Y, H> predict(X x);

	public H predict(X x, Y y);

	public H initializeLatent(X x, Y y);

	public void initialization(List<LabeledObject<X, Y>> data);

}
