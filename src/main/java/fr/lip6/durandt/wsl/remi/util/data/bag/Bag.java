package fr.lip6.durandt.wsl.remi.util.data.bag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Create a class to represent a bag. 
 * The bag is composed of a set of instances
 * 
 * @author Thibaut Durand - durand.tibo@gmail.com
 *
 */
public abstract class Bag<X> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8413262020273765251L;

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Name of the bag
	 */
	protected String name;

	/**
	 * Instances of the bag
	 */
	protected List<X> instances;


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructors
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor
	 */
	public Bag() {
		this("");
	}

	/**
	 * 
	 * @param name
	 */
	public Bag(String name) {
		this(name, new ArrayList<X>());
	}

	/**
	 * 
	 * @param name
	 * @param instances
	 */
	public Bag(String name, List<X> instances) {
		this.name = name;
		this.instances = instances;
	}



	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Add new instance to the bag at a given index
	 * @param index in the list
	 * @param instance vector representation
	 */
	public void addInstance(int index, X instance) {
		instances.add(index, instance);
	}

	/**
	 * Add new instance to the bag
	 * @param instance to add
	 */
	public void addInstance(X instance) {
		instances.add(instance);
	}

	/**
	 * Return the instance at given index 
	 * @param index
	 * @return instance at given index
	 */
	public X getInstance(int index) {
		return instances.get(index);
	}

	/**
	 * get the representation of i-th instance
	 * @param i
	 * @return representation of i-th instance
	 */
	public abstract double[] getInstanceRepresentation(int i);

	/**
	 * @return the list of instance features
	 */
	public List<X> getInstances() {
		return instances;
	}

	/**
	 * @return the name of the bag
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the number of instances in the bag
	 */
	public int getNumberOfInstances() {
		if(instances != null) {
			return instances.size();
		}
		else {
			return 0;
		}
	}

	/**
	 * Set instance feature at given index
	 * @param index index to set
	 * @param feature new feature to set
	 */
	public void setInstance(int index, X feature) {
		instances.set(index,feature);
	}

	/**
	 * Set the list of instance features
	 * @param features list of instance features
	 */
	public void setInstances(List<X> features) {
		this.instances = features;
	}

	/**
	 * Set the name of the bag
	 * @param name of the bag
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Bag [name=" + name + ", instances=" + instances.size() + "]";
	}
}
