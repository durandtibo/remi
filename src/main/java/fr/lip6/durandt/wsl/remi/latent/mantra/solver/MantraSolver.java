package fr.lip6.durandt.wsl.remi.latent.mantra.solver;

import java.io.Serializable;
import java.util.List;

import fr.lip6.durandt.wsl.remi.latent.mantra.model.PrimalMantraModel;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.solver.SolverOptions;

public abstract class MantraSolver<X, Y, H> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2905790934823410431L;

	public void evaluate(final PrimalMantraModel<X, Y, H> model,
			final List<LabeledObject<X, Y>> data,
			final List<LabeledObject<X, Y>> testData) {

		double primal = primalObjective(model, data);
		MantraUtils<X, Y, H> util = new MantraUtils<X, Y, H>();
		double trainError = util.empiricalLoss(model, data);

		if (testData != null) {
			double testError = util.empiricalLoss(model, testData);
			System.out.println("Primal objective= " + primal + ", Train error= " + trainError + ", Test error= " + testError);
		} else {
			System.out.println("Primal objective= " + primal + ", Train error= " + trainError);
		}
	}

	public abstract SolverOptions getOptions();

	public abstract PrimalMantraModel<X, Y, H> optimize(final PrimalMantraModel<X, Y, H> model,
			final List<LabeledObject<X, Y>> data);

	public abstract double primalObjective(final PrimalMantraModel<X, Y, H> model,
			final List<LabeledObject<X, Y>> data);

}
