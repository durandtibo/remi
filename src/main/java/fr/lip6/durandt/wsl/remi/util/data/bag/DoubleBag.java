package fr.lip6.durandt.wsl.remi.util.data.bag;

import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.remi.util.LabeledObject;

public class DoubleBag extends Bag<double[]> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2237733987572226174L;


	public static List<LabeledObject<double[], Integer>> bagToArray(List<LabeledObject<DoubleBag, Integer>> data) {
		List<LabeledObject<double[], Integer>> newData = new ArrayList<LabeledObject<double[], Integer>>();
		for(LabeledObject<DoubleBag, Integer> lo : data) {
			for(int i=0; i<lo.getPattern().getNumberOfInstances(); i++) {
				newData.add(new LabeledObject<double[], Integer>(lo.getPattern().getInstanceRepresentation(i).clone(), lo.getLabel()));
			}
		}
		return newData;
	}




	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	private String featureFile = null;


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	public DoubleBag() {
		super();
	}

	public String getFeatureFile() {
		return featureFile;
	}

	@Override
	public double[] getInstanceRepresentation(int i) {
		return getInstance(i);
	}

	public void setFeatureFile(String featureFile) {
		this.featureFile = featureFile;
	}

	@Override
	public String toString() {
		return "DoubleBag [featureFile=" + featureFile + ", " + super.toString() +  "]";
	}
}
