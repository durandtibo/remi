package fr.lip6.durandt.wsl.remi.util;

import java.io.Serializable;

public class LabeledObject<X, Y> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5114583279600185623L;

	/**
	 * pattern
	 */
	private X pattern = null;
	
	/**
	 * label
	 */
	private Y label = null;


	public LabeledObject(X pattern, Y label) {
		this.pattern = pattern;
		this.label = label;
	}

	public Y getLabel() {
		return label;
	}

	public X getPattern() {
		return pattern;
	}

	public void setLabel(Y label) {
		this.label = label;
	}

	public void setPattern(X pattern) {
		this.pattern = pattern;
	}

	@Override
	public String toString() {
		return "LabeledObject [pattern=" + pattern + ", label=" + label + "]";
	}

}

