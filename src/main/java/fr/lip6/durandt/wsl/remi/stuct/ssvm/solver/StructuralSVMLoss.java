package fr.lip6.durandt.wsl.remi.stuct.ssvm.solver;

import fr.lip6.durandt.wsl.remi.stuct.ssvm.model.PrimalStructuralSVM;
import fr.lip6.durandt.wsl.remi.util.solver.Loss;
import fr.lip6.durandt.wsl.remi.util.solver.StructuralLoss;

public class StructuralSVMLoss<X, Y> implements Loss<X, Y, PrimalStructuralSVM<X, Y>>, StructuralLoss<X, Y, PrimalStructuralSVM<X, Y>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6539845266633172081L;

	@Override
	public void add(PrimalStructuralSVM<X, Y> model, double[] a, X x, Y y) {
		model.add(a, x, y);
	}

	@Override
	public void add(PrimalStructuralSVM<X, Y> model, double[] a, X x, Y y, double gamma) {
		model.add(a, x, y, gamma);
	}

	@Override
	public double[] computeGradient(PrimalStructuralSVM<X, Y> model, X x, Y y) {
		// compute the loss augmented inference
		Y yStar = model.maxOracle(x, y);
		// compute the gradient of the loss
		double[] gradient = model.featureMap(x, yStar);
		model.sub(gradient, x, y);
		return gradient;
	}
	
	@Override
	public double[] computeGradient(PrimalStructuralSVM<X, Y> model, X x, Y y, Y yStar) {
		// compute the gradient of the loss
		double[] gradient = model.featureMap(x, yStar);
		model.sub(gradient, x, y);
		return gradient;
	}

	@Override
	public double error(PrimalStructuralSVM<X, Y> model, Y yTruth, Y y) {
		return model.loss(yTruth, y);
	}

	@Override
	public double evaluate(PrimalStructuralSVM<X, Y> model, X x, Y y) {
		// compute the loss augmented inference
		Y yStar = model.maxOracle(x, y);
		// compute the loss term
		return model.loss(y, yStar) + model.valueOf(x, yStar) - model.valueOf(x, y);
	}

	@Override
	public int getDimension(PrimalStructuralSVM<X, Y> model, X x, Y y) {
		return model.getDimension(x, y);
	}

	@Override
	public Y maxOracle(PrimalStructuralSVM<X, Y> model, X x, Y yStar) {
		return model.maxOracle(x, yStar);
	}

	@Override
	public void sub(PrimalStructuralSVM<X, Y> model, double[] a, X x, Y y) {
		model.sub(a, x, y);
	}



}
