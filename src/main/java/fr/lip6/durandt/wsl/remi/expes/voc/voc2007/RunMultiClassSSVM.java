package fr.lip6.durandt.wsl.remi.expes.voc.voc2007;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.lip6.durandt.wsl.remi.latent.mantra.Mantra;
import fr.lip6.durandt.wsl.remi.latent.mantra.model.MantraLatent;
import fr.lip6.durandt.wsl.remi.latent.mantra.model.MultiClassMantraModel4DoubleBag;
import fr.lip6.durandt.wsl.remi.latent.mantra.solver.cuttingplane.MantraWith1SCP;
import fr.lip6.durandt.wsl.remi.util.LabelLatent;
import fr.lip6.durandt.wsl.remi.util.LabeledObject;
import fr.lip6.durandt.wsl.remi.util.Pair;
import fr.lip6.durandt.wsl.remi.util.VectorOps;
import fr.lip6.durandt.wsl.remi.util.data.Evaluation;
import fr.lip6.durandt.wsl.remi.util.data.PreProcessingDoubleBag;
import fr.lip6.durandt.wsl.remi.util.data.bag.DoubleBag;
import fr.lip6.durandt.wsl.remi.util.solver.cuttingplane.OneSCPOptions;

public class RunMultiClassSSVM {

	public static void main(String[] args) {

		int dim = 4096;

		String pathDataset = "/local/durandt/simulation/VOC2007";
		String features = "imagenet-vgg-verydeep-16_layer_34_simple";
		String trainset = "trainval";
		String testset = "test";

		int scale = 100;
		
		double[] map = new double[VOC2007.numObjectCategories()];

		String fileTrain = pathDataset + "/files/csv/" + features + "/" + trainset + "_" + scale + ".csv";
		String fileTest = pathDataset + "/files/csv/" + features + "/" + testset + "_" + scale + ".csv";

		List<LabeledObject<DoubleBag, Integer[]>> trainAll = null;
		List<LabeledObject<DoubleBag, Integer[]>> testAll = null;

		PreProcessingDoubleBag<Integer[]> preprocess = new PreProcessingDoubleBag<Integer[]>();

		for(int i=0; i<VOC2007.numObjectCategories(); i++) {
			String cls = VOC2007.getCategory(i);

			if (trainAll == null) {
				// read train data
				trainAll = VOC2007.readDoubleBagCSV(fileTrain, dim);
				preprocess.normalization(trainAll);
			}

			if (testAll == null) {
				// read test data
				testAll = VOC2007.readDoubleBagCSV(fileTest, dim);
				preprocess.normalization(testAll);
			}

			List<LabeledObject<DoubleBag, Integer>> train = removeDifficultsExamples(trainAll, i);
			List<LabeledObject<DoubleBag, Integer>> test = removeDifficultsExamples(testAll, i);

			// define model
			MultiClassMantraModel4DoubleBag model = new MultiClassMantraModel4DoubleBag();

			// define solver options
			OneSCPOptions options = new OneSCPOptions();
			options.setC(1e3);
			options.setEpsilon(1e-2);
			options.setMaxCuttingPlanes(500);
			options.setnThreads(1);

			// define solver and train it
			MantraWith1SCP<DoubleBag, Integer, Integer> solver = new MantraWith1SCP<DoubleBag, Integer, Integer>(options);
			Mantra<DoubleBag, Integer, Integer> learner = new Mantra<DoubleBag, Integer, Integer>(solver);

			learner.train(model, train, test);

			List<Pair<Integer, Double>> predictionAndLabels = computePredictions(model, test);
			double ap = Evaluation.averagePrecision(predictionAndLabels);
			map[i] = ap;
			
			System.out.println(cls + " - ap=" + ap);
			
		}
		
		System.out.println("\nap per class " + Arrays.toString(map));
		System.out.println("map= " + VectorOps.mean(map));

	}

	public static List<LabeledObject<DoubleBag, Integer>> removeDifficultsExamples(List<LabeledObject<DoubleBag, Integer[]>> list, int indexClass) {
		List<LabeledObject<DoubleBag, Integer>> listWithoutDifficults = new ArrayList<LabeledObject<DoubleBag, Integer>>(list.size());
		for(LabeledObject<DoubleBag, Integer[]> example : list) {
			if(example.getLabel()[indexClass] != 0) {
				int label = example.getLabel()[indexClass] == 1 ? 1 : 0;
				listWithoutDifficults.add(new LabeledObject<DoubleBag, Integer>(example.getPattern(), label));
			}
		}
		return listWithoutDifficults;
	}

	public static List<Pair<Integer, Double>> computePredictions(MultiClassMantraModel4DoubleBag model, List<LabeledObject<DoubleBag, Integer>> list) {
		List<Pair<Integer, Double>> predictionAndLabels = new ArrayList<Pair<Integer, Double>>(list.size());
		for(int i=0; i<list.size(); i++) {
			LabelLatent<Integer, MantraLatent<Integer>> yPredict = model.predict(list.get(i).getPattern());
			double score = model.valueOf(list.get(i).getPattern(), yPredict);
			if (yPredict.getLabel() == 1) {
				predictionAndLabels.add(new Pair<Integer, Double>(list.get(i).getLabel(), score));
			}
			else {
				predictionAndLabels.add(new Pair<Integer, Double>(list.get(i).getLabel(), -score));
			}
		}
		return predictionAndLabels;
	}

}
