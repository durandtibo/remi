package fr.lip6.durandt.wsl.remi.util;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Class to perform vector operations.
 * 
 * @author Thibaut Durand - durand.tibo@gmail.com
 *
 */
public class VectorOps {

	public static double[] average(List<double[]> a) {
		double[] b = new double[a.get(0).length];
		for(double[] f : a) {
			VectorOps.add(b, f);
		}
		VectorOps.mul(b, 1.0 / (double)a.size());
		return b;
	}

	/**
	 * Performs an addition of 2 vectors and store the result in the first vector:
	 * a = a + b
	 * @param a first and output vector
	 * @param b second vector
	 */
	public static void add(double[] a, double[] b) {
		if(a.length != b.length) {
			throw new RuntimeException("The dimension of the arrays are not equals: " + a.length + " and " + b.length);
		}
		for(int i=0; i<a.length; i++) {
			a[i] += b[i];
		}
	}

	/**
	 * Performs a linear combination of 2 vectors and store the result in the first vector:
	 * a = a + lambda * b
	 * @param a first and output vector
	 * @param lambda weight of the second vector
	 * @param b second vector
	 */
	public static void add(double[] a, double[] b, double lambda) {
		if(a.length != b.length) {
			throw new RuntimeException("The dimension of the arrays are not equals: " + a.length + " and " + b.length);
		}
		for(int i=0; i<a.length; i++) {
			a[i] += lambda * b[i];
		}
	}

	/**
	 * Performs a linear combination of 2 vectors and store the result in the first vector:
	 * a = gamma * a + lambda * b
	 * @param a first and output vector
	 * @param b second vector
	 * @param gamma weight of the first vector
	 * @param lambda weight of the second vector
	 */
	public static void add(double[] a, double[] b, double gamma, double lambda) {
		if(a.length != b.length) {
			throw new RuntimeException("The dimension of the arrays are not equals: " + a.length + " and " + b.length);
		}
		for(int i=0; i<a.length; i++) {
			a[i] = gamma * a[i] + lambda * b[i];
		}
	}

	/**
	 * Performs a linear combination of 2 matrix and store the result in the first vector:
	 * a = gamma * a + lambda * b
	 * @param a first and output matrix
	 * @param b second matrix
	 * @param gamma weight of the first matrix
	 * @param lambda weight of the second matrix
	 */
	public static void add(double[][] a, double[][] b, double gamma, double lambda) {
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				a[i][j] = gamma * a[i][j] + lambda * b[i][j];
			}
		}
	}

	/**
	 * Performs a sum of 2 vectors and store the result in a new vector:
	 * c = a + b
	 * @param a first vector
	 * @param b second vector
	 * @return c = a + b
	 */
	public static double[] addn(double[] a, double[] b) {
		if(a.length != b.length) {
			throw new RuntimeException("The dimension of the arrays are not equals: " + a.length + " and " + b.length);
		}
		double[] c = new double[a.length];
		for(int i=0; i<a.length; i++) {
			c[i] = a[i] + b[i];
		}
		return c;
	}

	/**
	 * Concatenate a vector with a value
	 * @param vector
	 * @param value
	 * @return [vector, value]
	 */
	public static double[] addValeur(double[] vector, double value) {
		if(vector == null) return null;
		double[] newVector = new double[vector.length + 1];
		for(int i=0; i<vector.length; i++) {
			newVector[i] = vector[i];
		}
		newVector[vector.length] = value;
		return newVector;
	}

	/**
	 * Concatenation of 2 vectors
	 * @param a
	 * @param b
	 * @return [a,b]
	 */
	public static double[] concatenation(double[] a, double[] b) {

		if(a == null && b == null) {
			return null;
		}
		else if(a == null && b != null) {
			return b;
		}
		else if(a != null && b == null) {
			return a;
		}

		double[] c = new double[a.length + b.length];
		for(int i=0; i<a.length; i++) {
			c[i] = a[i];
		}
		for(int i=0; i<b.length; i++) {
			c[i+a.length] = b[i];
		}

		return c;
	}

	/**
	 * Compute the L2 distance between <b>a</b> and <b>b</b>
	 * @param a
	 * @param b
	 * @return
	 */
	public static double distanceL2(double[] a, double[] b) {
		double d=0;
		for(int i=0; i<a.length; i++) {
			d += (a[i]-b[i])*(a[i]-b[i]);
		}
		return Math.sqrt(d);
	}

	/**
	 * Computes the dot product between vector a and b
	 * @param a
	 * @param b
	 * @return &lt a, b &gt
	 */
	public static double dot(double[] a, double[] b) throws RuntimeException {
		if(a.length != b.length){
			throw new RuntimeException("The dimension of the arrays are not equals: " + a.length + " and " + b.length);
		}

		double s = 0;
		for(int i=0; i<a.length; i++){
			s += a[i]*b[i];
		}
		return s;
	}

	/**
	 * Computes the dot product between vector a and b
	 * @param a
	 * @param b
	 * @return &lt a, b &gt
	 */
	public static double dot(double[] a, Double[] b) {
		double s = 0;
		for(int i=0; i<a.length; i++) {
			s += a[i] * b[i];
		}
		return s;
	}

	/**
	 * Compute the dot product for each row and return the sum of each row
	 * @param a
	 * @param b
	 * @return
	 */
	public static double dot(double[][] a, double[][] b) {
		double s = 0;
		for(int i=0; i<a.length; i++) {
			s += dot(a[i], b[i]);
		}
		return s;
	}

	/**
	 * Computes the L1 norm of vector <b>a</b>
	 * @param a
	 * @return L1 norm of a
	 */
	public static double getNormL1(double[] a){
		double s = 0;
		for(int i=0; i<a.length; i++){
			s += Math.abs(a[i]);
		}
		return s;
	}

	/**
	 * Computes the L2 norm of vector <b>a</b>
	 * @param a
	 * @return L2 norm of a
	 */
	public static double getNormL2(double[] vector){
		double s = 0;
		for(int i=0; i<vector.length; i++){
			s += vector[i]*vector[i];
		}
		return Math.sqrt(s);
	}

	public static double getNormL2(double[][] vector){
		double s = 0;
		for(int i=0; i<vector.length; i++){
			for(int j=0; j<vector[i].length; j++){
				s += vector[i][j]*vector[i][j];
			}
		}
		return Math.sqrt(s);
	}

	/**
	 * return true if all the values of the array are >0
	 * @param a
	 * @return
	 */
	public static boolean isStrictPositive(double[] a) {
		for (int i=0; i<a.length; i++) {
			if (a[i] <= 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * return true if all the values of the array are >0
	 * @param a
	 * @return
	 */
	public static boolean isStrictPositive(int[] a) {
		for (int i=0; i<a.length; i++) {
			if (a[i] <= 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Compute the maximum of vector a
	 * @param a
	 * @return max_i a[i]
	 */
	public static double max(double[] a) {
		double max = -Double.MAX_VALUE;
		for(double v : a) {
			if(v>max) {
				max = v;
			}
		}
		return max;
	}

	/**
	 * Compute the argmax of vector a
	 * @param a
	 * @return argmax_i a[i]
	 */
	public static int argmax(double[] a) {
		double max = -Double.MAX_VALUE;
		int indexMax = -1;
		for(int i=0; i<a.length; i++) {
			double v = a[i];
			if(v>max) {
				max = v;
				indexMax = i;
			}
		}
		return indexMax;
	}

	/**
	 * Compute the maximum of vector a
	 * @param a
	 * @return
	 */
	public static Integer max(Integer[] a) {
		Integer max = -Integer.MAX_VALUE;
		for(Integer v : a) {
			if(v>max) {
				max = v;
			}
		}
		return max;
	}

	/**
	 * Compute the mean of vector <b>a</b>
	 * @param a
	 * @return
	 */
	public static double mean(double[] a) {
		double mean = sum(a);
		mean /= a.length;
		return mean;
	}

	/**
	 * Compute the average vector of 2 vectors
	 * @param vector1
	 * @param vector2
	 * @return (v1 + v2) / 2
	 */
	public static double[] mean(double[] v1, double[] v2){
		if(v1.length != v2.length){
			System.out.println("ERROR dot: vector1 :" + v1.length + "\t vector2: " + v2.length);
			System.exit(0);
		}

		double[] mean = new double[v1.length];
		for(int i=0; i<v1.length; i++){
			mean[i] = (v1[i] + v2[i]) / 2.;
		}
		return mean;
	}

	public static double min(double[] a) {
		double min = Double.MAX_VALUE;
		for(double v : a) {
			if(v<min) {
				min = v;
			}
		}
		return min;
	}

	public static double min(double[][] a) {
		double min = Double.MAX_VALUE;
		for(double[] t : a) {
			for(double v : t) {
				if(v<min) {
					min = v;
				}
			}
		}
		return min;
	}

	/**
	 * v = v * lambda
	 * @param v
	 * @param lambda
	 */
	public static void mul(double[] v, double lambda) {
		for(int i=0; i<v.length; i++){
			v[i] = v[i] * lambda;
		}
	}

	/**
	 * r = v * lambda
	 * @param v
	 * @param lambda
	 */
	public static double[] muln(double[] v, double lambda) {
		double[] a = new double[v.length];
		for(int i=0; i<v.length; i++){
			a[i] = v[i] * lambda;
		}
		return a;
	}

	/**
	 * Normalizes vector <b>a</b> with L1 norm. 
	 * The normalized vector is stored in <b>a</b>
	 * @param a
	 */
	public static void normL1(double[] a){
		double norm = getNormL1(a);
		if(norm != 0){
			for(int i=0; i<a.length; i++){
				a[i] = (a[i] / norm);
			}
		}
	}

	/**
	 * Normalizes vector <b>a</b> with L1 norm. 
	 * The normalized vector is stored in a new vector
	 * @param a
	 */
	public static double[] normL1n(double[] a){
		double norm = getNormL1(a);
		double[] b = new double[a.length];
		if(norm != 0){
			for(int i=0; i<a.length; i++){
				b[i] = (a[i] / norm);
			}
		}
		return b;
	}

	public static void normL2(double[] vector){
		double norm = getNormL2(vector);
		if(norm != 0){
			for(int i=0; i<vector.length; i++){
				vector[i] = (vector[i] / norm);
			}
		}
	}

	public static void normL2(double[][] vector){
		double norm = getNormL2(vector);
		if(norm != 0){
			for(int i=0; i<vector.length; i++){
				for(int j=0; j<vector[i].length; j++){
					vector[i][j] = (vector[i][j] / norm);
				}
			}
		}
	}

	/**
	 * Compute Platt scaling
	 * x[i] = 1 / (1 + exp(a*x[i] + b))
	 * @param x
	 * @param a
	 * @param b
	 * @return
	 */
	public static void plattScaling(double[] x, double a, double b) {
		for(int i=0; i<x.length; i++) {
			x[i] = 1. / (1. + Math.exp(a*x[i] + b));
		}
	}

	public static void plattScaling(List<double[]> l, double a, double b) {
		for(double[] x : l) {
			plattScaling(x, a, b);
		}
	}

	/**
	 * Compute the p-norm of vector x
	 * @param x n-dimension vector 
	 * @param p 
	 * @return (\sum_{i=1}^n |x[i]|^p)^(1/p)
	 */
	public static double pnorm(double[] x, double p) {
		if(p>=1) {
			double np = 0.;
			for(double xi : x) {
				np += Math.pow(Math.abs(xi), p);
			}
			return Math.pow(np,1/p);
		}
		else {
			System.out.println("Error: p-nom  p>=1 and p= " + p);
			return 0;
		}
	}

	public static double stddev(double[] f) {
		double m = mean(f);
		double stddev = 0;
		for(double s : f) {
			stddev += (s - m)*(s - m);
		}
		stddev = Math.sqrt(stddev/f.length);
		return stddev;
	}

	/**
	 * Convert the string of the feature in an array of double. 
	 * Verify if the dimension is correct with the provide dimension (dimx).
	 * @param s string of the feature
	 * @param dim dimension of the feature
	 * @return feature in an array of double
	 */
	public static double[] stringToArrayDouble(String s, int dim) {
		double[] feature = new double[dim];
		StringTokenizer st = new StringTokenizer(s);
		if(st.countTokens() != dim) {
			System.out.println("Error - number of tokens incorrect " + st.countTokens() + " vs " + dim);
			System.exit(0);
		}
		// Convert the string in array of double
		for(int i=0; i<dim; i++) {
			feature[i] = Double.parseDouble(st.nextToken());
		}
		return feature;
	}

	/**
	 * Performs an subtraction of 2 vectors and store the result in the first vector:
	 * a = a - b
	 * @param a first and output vector
	 * @param b second vector
	 */
	public static void sub(double[] a, double[] b) {
		if(a.length != b.length) {
			throw new RuntimeException("The dimension of the arrays are not equals: " + a.length + " and " + b.length);
		}
		for(int i=0; i<a.length; i++) {
			a[i] -= b[i];
		}
	}

	/**
	 * Performs a subtraction of 2 vectors and store the result in a new vector:
	 * c = a - b
	 * @param a first vector
	 * @param b second vector
	 */
	public static double[] subn(double[] a, double[] b) {
		if(a.length != b.length) {
			throw new RuntimeException("The dimension of the arrays are not equals: " + a.length + " and " + b.length);
		}
		double[] c = new double[a.length];
		for(int i=0; i<a.length; i++) {
			c[i] = a[i] - b[i];
		}
		return c;
	}

	/**
	 * Compute the sum 
	 * @param a
	 * @return sum_i a[i]
	 */
	public static double sum(double[] a) {
		double sum = 0.0;
		for(int i=0; i<a.length; i++) {
			sum += a[i];
		}
		return sum;
	}

	public static double[] truncate(double[] a, int dim) {
		dim = Math.min(dim, a.length);
		double[] b = new double[dim];
		for(int i=0; i<dim; i++) {
			b[i] = a[i];
		}
		return b;
	}
}
